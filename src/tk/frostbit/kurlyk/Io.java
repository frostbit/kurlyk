package tk.frostbit.kurlyk;

import java.io.*;
import java.util.Scanner;

public class Io {

    public static String readStream(InputStream is) {
        return readStream(is, "CP1251");
    }

    public static String readStream(InputStream is, String encoding) {
        final Scanner scn = new Scanner(is, encoding);
        scn.useDelimiter("\\a");
        return scn.next();
    }

    public static String readFile(String fileName) throws IOException {
        final InputStream is = new FileInputStream(fileName);
        final String data = readStream(is, "UTF-8");
        is.close();
        return data;
    }

}
