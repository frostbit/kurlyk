package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.Type;
import tk.frostbit.kurlyk.core.Value;

import java.util.*;

public class CompileContext {

    public final KurMethod method;
    public final KurClass clazz;
    public final Stack<Type> types = new Stack<>();

    public CompileContext(KurMethod method, KurClass clazz) {
        this.method = method;
        this.clazz = clazz;
    }

    public int resolveLocal(String name) {
        final List<String> locals = method.localNames;
        for (int i = 0; i < locals.size(); i++) {
            if (locals.get(i).equals(name)) {
                return i + 1;
            }
        }
        return 0;
    }

    public void addLocal(Type type, String name) {
        method.localTypes.add(type);
        method.localNames.add(name);
    }

    public Type localType(int index) {
        return method.localTypes.get(index - 1);
    }

    public Type findInstanceField(String name) {
        return clazz.instanceFields.get(name);
    }

    public Type findStaticField(String name) {
        final Value statField = clazz.staticFields.get(name);
        return (statField == null) ? null : statField.getType();
    }

    public KurMethod findMethod(String signature) {
        return clazz.methods.get(signature);
    }

}
