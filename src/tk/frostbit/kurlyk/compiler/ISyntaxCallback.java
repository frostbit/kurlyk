package tk.frostbit.kurlyk.compiler;

public interface ISyntaxCallback {
    void onImport(Lexem lexem);

    void onClassDef(Lexem lexem);

    void onClassCommit(Lexem lexem);

    void onIdent(Lexem lexem);

    void onMemberDef(Lexem lexem);

    void onFieldCommit(Lexem lexem);

    void onInit(Lexem lexem);

    void onCommitUn(Lexem lexem);

    void onDecl(Lexem lexem);

    void onStatic(Lexem lexem);

    void onType(Lexem lexem);

    void onArrayType(Lexem lexem);

    void onTypeCommit(Lexem lexem);

    void onEntityName(Lexem lexem);

    void onCtor(Lexem lexem);

    void onArgsDef(Lexem lexem);

    void onArgDef(Lexem lexem);

    void onMethStart(Lexem lexem);

    void onMethCommit(Lexem lexem);

    void onMember(Lexem lexem);

    void onIndex(Lexem lexem);

    void onNewObj(Lexem lexem);

    void onNewArray(Lexem lexem);

    void onCtorCall(Lexem lexem);

    void onCall(Lexem lexem);

    void onArgs(Lexem lexem);

    void onArgsCommit(Lexem lexem);

    void onUnOp(Lexem lexem);

    void onConst(Lexem lexem);

    void onBinOp(Lexem lexem);

    void onCommitBin(Lexem lexem);

    void onBlockCommit(Lexem lexem);

    void onBlock(Lexem lexem);

    void onReturnExp(Lexem lexem);

    void onReturnVoid(Lexem lexem);

    void onAssign(Lexem lexem);

    void onLocalDef(Lexem lexem);

    void onInitLocal(Lexem lexem);

    void onIfElse(Lexem lexem);

    void onIf(Lexem lexem);

    void onWhile(Lexem lexem);

    void onBreak(Lexem lexem);

    void onSwitch(Lexem lexem);

    void onCase(Lexem lexem);

    void onSwitchCommit(Lexem lexem);
}
