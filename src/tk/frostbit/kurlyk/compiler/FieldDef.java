package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Type;

public class FieldDef extends MemberDef {

    public FieldDef(String name, Type type, boolean isStatic) {
        super(name, type, null, isStatic);
        if (type.getBase() == BaseType.VOID) {
            throw new CompileError("Illegal void type for field " + name);
        }
    }

}
