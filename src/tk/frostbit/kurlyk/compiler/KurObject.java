package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.*;

import java.util.*;

class KurObject implements IObject {

    private final Map<String, Value> fields = new HashMap<>();

    private final KurClass clazz;

    KurObject(KurClass clazz) {
        this.clazz = clazz;
        for (Map.Entry<String, Type> entry : clazz.instanceFields.entrySet()) {
            fields.put(entry.getKey(), entry.getValue().newValue());
        }
    }

    @Override
    public IClass clazz() {
        return clazz;
    }

    @Override
    public Value getField(String name) {
        return fields.get(name);
    }

    @Override
    public String toString() {
        return clazz.getName() + "@" + Integer.toHexString(hashCode());
    }

}
