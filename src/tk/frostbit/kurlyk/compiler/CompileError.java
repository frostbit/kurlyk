package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.compiler.ast.*;
import tk.frostbit.kurlyk.core.IClass;
import tk.frostbit.kurlyk.core.Type;
import tk.frostbit.kurlyk.core.Value;

public class CompileError extends RuntimeException {

    public CompileError(String message) {
        super(message);
    }

    public static CompileError makeExpressionExpected(AstNode node) {
        return new CompileError("Expression expected; got " + node);
    }

    public static CompileError makeIllegalCall(AstNode node) {
        return new CompileError("Illegal call of " + node);
    }

    public static CompileError makeMissingReturn(KurMethod method, IClass clazz) {
        return new CompileError("Missing return in "
                + method.definition.getSignature() + " (" + clazz.getName() + ")");
    }

    public static CompileError makeAssignmentToCall(CallNode node) {
        return new CompileError("Assignment to call of method " + node.methodName);
    }

    public static CompileError makeIllegalType(Type type, AstNode node) {
        return new CompileError("Illegal type: " + type);
    }

    public static CompileError makeTypeMismatch(Type dst, Type src, AstNode node) {
        return new CompileError("Type mismatch: expected " + dst + ", got " + src);
    }

    public static CompileError makeMethodNotFound(String method, IClass clazz, AstNode node) {
        return new CompileError("Method " + method + " is not found in " + clazz.getName());
    }

    public static CompileError makeFieldNotFound(String field, IClass clazz, AstNode node) {
        return new CompileError("Field " + field + " is not found in " + clazz.getName());
    }

    public static CompileError makeStaticInstAccess(String destName, AstNode node) {
        return new CompileError("Illegal access to " + destName + " from static context");
    }

    public static CompileError makeUnknownIdentifier(IdentNode node) {
        return new CompileError("Unknown identifier: " + node.ident);
    }

    public static CompileError makeNotAppiable(String opSign, Type argType, AstNode node) {
        return new CompileError("Operation " + opSign + " cannot be applied to " + argType);
    }

    public static CompileError makeNotAppiable(String opSign,
                                               Type leftType, Type rightType, AstNode node) {
        return new CompileError("Operation " + opSign + " cannot be applied to "
                + leftType + " and " + rightType);
    }

}
