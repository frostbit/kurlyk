package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.*;

import java.util.*;

/**
 * Класс, написанный на Kurlyk.
 */
class KurClass implements IClass {

    final Map<String, Value> staticFields = new HashMap<>();
    final Map<String, Type> instanceFields = new HashMap<>();
    final Map<String, KurMethod> methods = new HashMap<>();
    final List<KurMethod> methodList = new ArrayList<>();

    private final String name;

    public KurClass(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isNative() {
        return false;
    }

    @Override
    public IMethod getMethod(String signature) {
        return methods.get(signature);
    }

    @Override
    public Value getStaticField(String name) {
        return staticFields.get(name);
    }

    @Override
    public Value<IObject> newInstance() {
        return new Value<>(BaseType.OBJECT, new KurObject(this));
    }

    @Override
    public String toString() {
        return name;
    }

}
