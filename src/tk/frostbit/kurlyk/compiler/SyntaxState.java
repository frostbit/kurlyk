package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Type;

import java.util.*;

class SyntaxState {

    boolean modStatic;

    BaseType baseType;
    int arrayArity;
    String entityName;

    List<Type> argTypes;
    List<String> methArgs;

}
