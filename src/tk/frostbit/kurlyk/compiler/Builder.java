package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.StringUtils;
import tk.frostbit.kurlyk.WtlTransfer;

import java.io.*;
import java.util.*;

/**
 * Превращает выхлоп WTL в более-менее нормальный анализатор.
 */
public class Builder {

    private static final String TARGET_PATH =
            "C:\\Users\\fRoStBiT\\Projects\\Java\\Kurlyk\\" +
                    "src\\tk\\frostbit\\kurlyk\\compiler\\GeneratedCompiler.java";

    public static void main(String[] args) throws Exception {
        final Properties props = new Properties();
        props.load(Builder.class.getResourceAsStream("/builder.properties"));

        final String input = WtlTransfer.getFile(props.getProperty("ANALYSER_WTL_PATH"));
        final StringBuilder result = new StringBuilder(input);

        patchAnalyzer(result);

        final FileOutputStream fos = new FileOutputStream(TARGET_PATH);
        fos.write(result.toString().getBytes("UTF-8"));
        fos.close();
    }

    private static void patchAnalyzer(StringBuilder result) {
        result.insert(0, "package " + Builder.class.getPackage().getName() + ";\n\n");
        // Вырезание класса для запуска
        StringUtils.cut(result, "//Part_7", "//end of Part_7");
        // Добавление пользовательского кода
        StringUtils.insertAfter(result, "//Part_5",
                "\nISyntaxCallback sc = SyntaxCallback.getInstance();");
        // Переименование сущностей
        StringUtils.replaceWord(result, "lexem", "_Replace");
        StringUtils.replaceWord(result, "Lexem", "lexem");
        StringUtils.replaceWord(result, "_Replace", "Lexem");
        StringUtils.replaceWord(result, "parser", "SyntaxAnalyzer");
        StringUtils.replaceWord(result, "lexAnalyzer", "LexAnalyzer");
        StringUtils.replaceWord(result, "textReader", "TextReader");
        StringUtils.replaceWord(result, "textOfWord", "text");
        // Добавление возврата ошибочной лексеммы
        StringUtils.replaceAll(result, 0, "public boolean parse()", "public Lexem parse()");
        final int parseStart = result.indexOf("parse()");
        StringUtils.replaceAll(result, parseStart, "return false;", "return currentLexem;");
        StringUtils.replaceAll(result, parseStart, "return true;", "return null;");
        StringUtils.replaceAll(result, parseStart,
                "return (curWordIndex==0?true:false);",
                "return curWordIndex==0?null:currentLexem;");
        // Добавление номеров строк в лексеммы
        StringUtils.insertAfter(result, "class Lexem{", "\n public int line;");
        StringUtils.insertAfter(result, "class TextReader{", "\n int line = 1;");
        StringUtils.replaceAll(result, 0, "return lexem;",
                "lexem.line=lexAcceptor.inp.line;\n  return lexem;");
        StringUtils.replaceAll(result, 0, "return (pos<l?(int)inStr.charAt(pos++):-1);",
                "  if (pos < inStr.length()) {\n" +
                "       char c = inStr.charAt(pos++);\n" +
                "       if (c == '\\n') line++;\n" +
                "       return c;\n" +
                "   } else {\n" +
                "       return -1;\n" +
                "   }");
    }

}
