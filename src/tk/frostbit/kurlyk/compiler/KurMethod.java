package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.runtime.StackFrame;

import java.util.*;

public class KurMethod implements IMethod {

    final List<Type> localTypes = new ArrayList<>();
    final List<String> localNames = new ArrayList<>();
    final int argCount;

    private final Type returnType;
    private final boolean isStatic;

    final MethodDef definition;

    public int entryPoint;

    public KurMethod(MethodDef definition) {
        this.definition = definition;
        returnType = definition.getType();
        isStatic = definition.isStatic();

        if (!isStatic) {
            localNames.add(Signature.CURRENT_OBJECT_IDENT);
            localTypes.add(Type.base(BaseType.OBJECT));
        }

        for (int i = 0; i < definition.getArgNames().size(); i++) {
            final String argName = definition.getArgNames().get(i);
            final Type argType = definition.getArgTypes().get(i);
            localNames.add(argName);
            localTypes.add(argType);
        }

        argCount = localTypes.size();
    }

    public String getLocalName(int id) {
        return localNames.get(id - 1);
    }

    @Override
    public int call(int argc, Stack<Value<?>> stack, StackFrame outFrame) {
        final Value<?>[] locals = new Value[localTypes.size()];
        if (!isStatic) {
            // Кроме прочих аргументов сохраняем
            // в локальные переменные ссылку на объект
            argc++;
        }
        for (int i = 0; i < locals.length; i++) {
            locals[i] = localTypes.get(i).newValue();
            if (i < argc) {
                locals[i].assign(stack.pop());
            }
        }
        outFrame.locals = locals;
        return entryPoint;
    }

    @Override
    public Value callNative(int argc, Stack<Value<?>> stack) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNative() {
        return false;
    }

    @Override
    public boolean isStatic() {
        return isStatic;
    }

    @Override
    public Type getType() {
        return returnType;
    }
}
