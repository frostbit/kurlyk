package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.framework.Framework;

import java.util.*;

public class Compiler {

    private final Program prog;
    private final Stack<KurClass> compileTarget = new Stack<>();

    public Compiler() {
        prog = new Program();
        Framework.bootstrap(prog);
    }

    public Program getProgram() {
        return prog;
    }

    public void loadClass(ClassDef classDef) {
        final KurClass clazz = new KurClass(classDef.getName());
        prog.registerClass(clazz);
        compileTarget.push(clazz);
        for (FieldDef field : classDef.getFields()) {
            if (field.isStatic()) {
                final Value<?> value = field.getType().newValue();
                clazz.staticFields.put(field.getName(), value);
            } else {
                clazz.instanceFields.put(field.getName(), field.getType());
            }
        }
        // Добавление всех метдов в класс до их компиляции
        for (MethodDef methodDef : classDef.getMethods()) {
            final KurMethod method = new KurMethod(methodDef);
            clazz.methods.put(methodDef.getSignature(), method);
            clazz.methodList.add(method);
        }
    }

    public void compileClasses() {
        while (!compileTarget.empty()) {
            compileClass(compileTarget.pop());
        }
    }

    private void compileClass(KurClass clazz) {
        for (KurMethod method : clazz.methodList) {
            final String fullSign = Signature.globalMethod(clazz.getName(), method.definition);
            final int symbolId = prog.registerSymbol(fullSign);
            method.entryPoint = prog.size();
            prog.addInstruction(Opcode.SOURCE, symbolId, 0, 0);
            method.definition.getAstRoot().compile(prog, new CompileContext(method, clazz));
            if (prog.pc(prog.size() - 1).op != Opcode.RET) {
                if (method.getType().getBase() != BaseType.VOID) {
                    throw CompileError.makeMissingReturn(method, clazz);
                }
                prog.addInstruction(new Pcode(Opcode.RET));
            }
            optimize(method.entryPoint);
        }
    }

    private void optimize(int start) {
        for (int i = start; i < prog.size(); i++) {
            final Pcode pc = prog.pc(i);
            final Pcode pc2 = (i < prog.size() - 1) ? prog.pc(i + 1) : null;
            final Pcode pc3 = (i < prog.size() - 2) ? prog.pc(i + 2) : null;
            /*
             * Оптимизация ситуации
             * MOV #1 - @
             * MOV #2 - @
             * BINOP @ @ @
             */
            if (pc.op == Opcode.MOV && pc2 != null && pc2.op == Opcode.MOV) {
                if (pc.dest == Pcode.VAL_STACK && pc2.dest == Pcode.VAL_STACK) {
                    if (pc3 != null && pc3.op.arg1type == ArgType.VALUE
                            && pc3.op.arg2type == ArgType.VALUE) {
                        if (pc3.arg1 == Pcode.VAL_STACK && pc3.arg2 == Pcode.VAL_STACK) {
                            pc3.arg1 = pc2.arg1;
                            pc3.arg2 = pc.arg1;
                            pc.op = Opcode.NOP;
                            pc2.op = Opcode.NOP;
                        }
                    }
                }
            }
            /*
             * Оптимизация ситуации
             * MOV #1 - @
             * UNOP @ - @
             */
            if (pc.op == Opcode.MOV && pc.dest == Pcode.VAL_STACK) {
                if (pc2 != null && pc2.op.arg1type == ArgType.VALUE) {
                    if (pc2.arg1 == Pcode.VAL_STACK) {
                        pc2.arg1 = pc.arg1;
                        pc.op = Opcode.NOP;
                    }
                }
            }
            /*
             * Оптимизация ситуации
             * ANYOP ? ? @
             * MOV @ - #1
             */
            if (pc.op.hasResult && pc.dest == Pcode.VAL_STACK) {
                if (pc2 != null && pc2.op == Opcode.MOV && pc2.arg1 == Pcode.VAL_STACK) {
                    pc.dest = pc2.dest;
                    pc2.op = Opcode.NOP;
                }
            }
        }
        // Вырезание NOP
        for (int i = start; i < prog.size(); i++) {
            final Pcode pc = prog.pc(i);
            if (pc.op.arg1type == ArgType.ADDRESS) {
                int offset = 0;
                for (int j = start; j < pc.arg1; j++) {
                    if (prog.pc(j).op == Opcode.NOP) {
                        offset++;
                    }
                }
                pc.arg1 -= offset;
            }
        }
        for (int i = start; i < prog.size(); ) {
            if (prog.pc(i).op == Opcode.NOP) {
                prog.removeInstruction(i);
            } else {
                i++;
            }
        }
    }

}