package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.compiler.ast.BlockNode;
import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Signature;
import tk.frostbit.kurlyk.core.Type;

import java.util.*;

public class ClassDef {

    private final String name;
    private final MethodDef initializer;
    private final MethodDef staticInitializer;
    private final List<FieldDef> fields = new ArrayList<>();
    private final List<MethodDef> methods = new ArrayList<>();

    ClassDef(String name) {
        this.name = name;
        final List<Type> argTypes = Collections.emptyList();
        final List<String> argNames = Collections.emptyList();
        initializer = new MethodDef(
                Signature.INIT_NAME, Type.base(BaseType.VOID),
                argTypes, argNames, false);
        initializer.setAstRoot(new BlockNode());
        addMethod(initializer);
        staticInitializer = new MethodDef(
                Signature.STATIC_INIT_NAME, Type.base(BaseType.VOID),
                argTypes, argNames, true);
        staticInitializer.setAstRoot(new BlockNode());
        addMethod(staticInitializer);
    }

    public String getName() {
        return name;
    }

    public void addField(FieldDef field) {
        addMember(field, fields);
    }

    public void addMethod(MethodDef method) {
        addMember(method, methods);
    }

    private <T extends MemberDef> void addMember(T member, List<T> dest) {
        final MemberDef prevDef = findMember(member.getName(), member.getArgTypes());
        if (prevDef != null) {
            throw new CompileError(
                    "Member definition conflict: " + member + " and " + prevDef);
        }
        dest.add(member);
    }

    public MemberDef findMember(String name, List<Type> args) {
        final String signature = Signature.method(name, args);
        final List<? extends MemberDef> members = (args == null) ? fields : methods;
        for (MemberDef m : members) {
            if (m.getSignature().equals(signature)) {
                return m;
            }
        }
        return null;
    }

    public List<FieldDef> getFields() {
        return fields;
    }

    public List<MethodDef> getMethods() {
        return methods;
    }

    public List<MemberDef> getMembers() {
        final List<MemberDef> members = new ArrayList<>();
        members.addAll(fields);
        members.addAll(methods);
        return members;
    }

    public MethodDef getInitializer() {
        return initializer;
    }

    public MethodDef getStaticInitializer() {
        return staticInitializer;
    }

}
