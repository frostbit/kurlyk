package tk.frostbit.kurlyk.compiler;

import java.util.*;

class Keywords {

    private static final String[] KEYWORDS_ARRAY = {
            "import",
            "object",
            "static",
            "set",
            "if",
            "else",
            "while",
            "loop",
            "switch",
            "by",
            "do",
            "break",
            "new",
            "void",
            "return",
            "true",
            "false",
            "null",
            "this",
            "bool",
            "signed",
            "number",
            "sign",

            "int"
    };

    private static final Set<String> KEYWORDS;

    private static final Map<String, String> ALIASES = new HashMap<String, String>() {
        {
            put("int", "signed");
        }
    };

    static {
        KEYWORDS = new HashSet<>();
        Collections.addAll(KEYWORDS, KEYWORDS_ARRAY);
    }

    public static boolean contains(String word) {
        return KEYWORDS.contains(word);
    }

    public static void processLexem(Lexem lexem) {
        final StringBuffer word = lexem.text;
        final String lowerCase = word.toString().toLowerCase();
        if (contains(lowerCase)) {
            word.setLength(0);
            word.append(lowerCase);
        }

        final String key = word.toString();
        if (ALIASES.containsKey(key)) {
            word.setLength(0);
            word.append(ALIASES.get(key));
        }
    }

}
