package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Program;

public class IfNode extends AstNode {

    public final ExpressionNode condition;
    public final AstNode body;
    public final AstNode elseBody;

    public IfNode(AstNode condition, AstNode body, AstNode elseBody) {
        this.condition = condition.asExpression();
        this.body = body;
        this.elseBody = elseBody;
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        condition.compile(prog, context);
        checkTypes(BaseType.BOOL.asType(), context.types.pop());
        final int jfAdr = prog.size();
        prog.addInstruction(Opcode.JF, 0, 0, 0);
        body.compile(prog, context);
        if (elseBody == null) {
            prog.pc(jfAdr).arg1 = prog.size();
        } else {
            final int jmpAdr = prog.size();
            prog.addInstruction(Opcode.JMP, 0, 0, 0);
            prog.pc(jfAdr).arg1 = prog.size();
            elseBody.compile(prog, context);
            prog.pc(jmpAdr).arg1 = prog.size();
        }
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        condition.onVisit(visitor, deep + 1);
        body.onVisit(visitor, deep + 1);
        if (elseBody != null) {
            elseBody.onVisit(visitor, deep + 1);
        }
    }

}
