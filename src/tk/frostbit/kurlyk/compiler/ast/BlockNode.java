package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.Program;

import java.util.*;

public class BlockNode extends AstNode {

    public final List<AstNode> nodes;

    public BlockNode() {
        this.nodes = new ArrayList<>();
    }

    public BlockNode(List<AstNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        for (AstNode node : nodes) {
            node.compile(prog, context);
        }
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        visitNodes(visitor, deep + 1);
    }

    protected final void visitNodes(Visitor visitor, int deep) {
        for (AstNode node : nodes) {
            node.onVisit(visitor, deep);
        }
    }

}
