package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.*;
import tk.frostbit.kurlyk.core.*;

public class CallNode extends ExpressionNode {

    public final ExpressionNode object;
    public final String methodName;
    public final ArgListNode args;

    public CallNode(AstNode method, AstNode args) {
        if (method instanceof MemberNode) {
            final MemberNode member = (MemberNode) method;
            object = member.object;
            methodName = member.name;
        } else if (method instanceof IdentNode) {
            final IdentNode ident = (IdentNode) method;
            object = null;
            methodName = ident.ident;
        } else {
            throw CompileError.makeIllegalCall(method);
        }
        this.args = (ArgListNode) args;
    }

    @Override
    public Type compileExpression(Program prog, CompileContext context) {
        args.compile(prog, context);
        final int argc = args.nodes.size();
        final String methSignature = Signature.method(methodName, argc);
        final Type returnType;
        final Pcode callPcode;
        if (object == null) {
            final IMethod member = context.findMethod(methSignature);
            if (member == null) {
                throw CompileError.makeMethodNotFound(methSignature, context.clazz, this);
            } else {
                if (member.isStatic()) {
                    // Вызов статического метода в текущем классе
                    callPcode = buildStatic(context.clazz, methSignature, argc, prog);
                } else {
                    if (context.method.isStatic()) {
                        throw CompileError.makeStaticInstAccess(methodName, this);
                    } else {
                        // Вызов метода текущего объекта
                        // Помещение в стек ссылки на текущий объект (this)
                        prog.addInstruction(Opcode.MOV, 1, 0, Pcode.VAL_STACK);
                        context.types.push(BaseType.OBJECT.asType());
                        callPcode = buildInstance(methSignature, argc, prog);
                    }
                }
                returnType = member.getType();
            }
        } else if (object instanceof IdentNode) {
            final String ident = ((IdentNode) object).ident;
            final IClass clazz = prog.getClass(ident);
            if (clazz != null) {
                final IMethod method = clazz.getMethod(methSignature);
                if (method == null) {
                    throw CompileError.makeMethodNotFound(methSignature, clazz, this);
                } else {
                    // Вызов статического метода в другом классе
                    callPcode = buildStatic(clazz, methSignature, argc, prog);
                    returnType = method.getType();
                }
            } else {
                // Вызов метода другого объекта
                object.compile(prog, context);
                callPcode = buildInstance(methSignature, argc, prog);
                returnType = Type.UNDEFINED;
            }
        } else {
            // Вызов метода другого объекта
            object.compile(prog, context);
            callPcode = buildInstance(methSignature, argc, prog);
            returnType = Type.UNDEFINED;
        }
        if (returnType.getBase() == BaseType.VOID) {
            callPcode.dest = 0;
        }
        if (callPcode.op == Opcode.CALLVIRT) {
            // Снятие со стека типа вызываемого объекта
            context.types.pop();
        }
        for (int i = 0; i < argc; i++) {
            // Снятие со стека типов аргументов
            context.types.pop();
        }
        prog.addInstruction(callPcode);
        return returnType;
    }

    private Pcode buildInstance(String methodSignature, int argc, Program prog) {
        final int symbol = prog.registerSymbol(methodSignature);
        return new Pcode(Opcode.CALLVIRT, symbol, argc, Pcode.VAL_STACK);
    }

    private Pcode buildStatic(IClass clazz, String methodSignature, int argc, Program prog) {
        final String globalSignature = Signature.globalMethod(clazz.getName(), methodSignature);
        final int symbol = prog.registerSymbol(globalSignature);
        return new Pcode(Opcode.CALL, symbol, argc, Pcode.VAL_STACK);
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        if (object != null) {
            object.onVisit(visitor, deep + 1);
        }
        args.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        String str = super.toString() + " " + methodName;
        if (object == null) {
            str += " (this)";
        }
        return str;
    }
}
