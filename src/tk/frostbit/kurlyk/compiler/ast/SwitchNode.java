package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.*;

import java.util.*;

public class SwitchNode extends BlockNode {

    public final ExpressionNode expression;

    public SwitchNode(AstNode expression, List<AstNode> nodes) {
        super(nodes);
        this.expression = expression.asExpression();
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        expression.compile(prog, context);
        final Type exprType = context.types.pop();
        final int start = prog.size();
        int caseCount = 0;
        for (AstNode node : nodes) {
            if (node instanceof CaseNode) {
                caseCount++;
                prog.addInstruction(Opcode.JE, 0, 0, 0);
            }
        }
        prog.addInstruction(Opcode.JMP, 0, 0, 0);
        final List<Integer> breaks = new ArrayList<>();
        int caseIndex = 0;
        for (AstNode node : nodes) {
            if (node instanceof CaseNode) {
                final CaseNode caseNode = (CaseNode) node;
                final int constId = prog.registerConst(caseNode.constant);
                checkTypes(exprType, caseNode.constant.getType());
                final Pcode jumpInstr = prog.pc(start + caseIndex);
                jumpInstr.arg1 = prog.size();
                jumpInstr.arg2 = constId;
                caseNode.compile(prog, context);
                caseIndex++;
            } else if (node instanceof BreakNode) {
                breaks.add(prog.size());
                node.compile(prog, context);
            } else {
                throw new IllegalStateException("Illegal child node: " + node);
            }
        }
        final int end = prog.size();
        prog.pc(start + caseCount).arg1 = end;
        for (int breakAdr : breaks) {
            prog.pc(breakAdr).arg1 = end;
        }
        prog.addInstruction(Opcode.MOV, Pcode.VAL_STACK, 0, 0);
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        visitor.visit(this, deep);
        expression.onVisit(visitor, deep + 1);
        visitNodes(visitor, deep + 1);
    }
}
