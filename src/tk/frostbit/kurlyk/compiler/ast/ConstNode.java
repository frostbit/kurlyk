package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.core.Program;

public class ConstNode extends ExpressionNode {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String NULL = "null";

    public final Value value;

    public ConstNode(String str) {
        value = parseConst(str);
    }

    @Override
    public Type compileExpression(Program prog, CompileContext context) {
        final int id = prog.registerConst(value);
        final Pcode instr = new Pcode(Opcode.MOV, id, 0, Pcode.VAL_STACK);
        prog.addInstruction(instr);
        return value.getType();
    }

    private Value parseConst(String str) {
        switch (str) {
            case NULL:
                return Value.NULL;
            case TRUE:
                return Value.TRUE;
            case FALSE:
                return Value.FALSE;
        }
        try {
            final int value = Integer.decode(str);
            return new Value<>(BaseType.INTEGER, value);
        } catch (NumberFormatException ignored) { }
        try {
            final double value = Double.parseDouble(str);
            return new Value<>(BaseType.NUMBER, value);
        } catch (NumberFormatException ignored) { }
        if (str.startsWith("'")) {
            final char firstChar = str.charAt(1);
            if (firstChar == '\\') {
                final char value;
                switch (str.charAt(2)) {
                    case '\\':
                        value = '\\';
                        break;
                    case '\'':
                        value = '\'';
                        break;
                    case 'n':
                        value = '\n';
                        break;
                    case 't':
                        value = '\t';
                        break;
                    default:
                        throw new CompileError("Illegal escape character: " + str);
                }
                return new Value<>(BaseType.SIGN, value);
            } else {
                return new Value<>(BaseType.SIGN, firstChar);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        final Object val = (value == null) ? null : value.getPrimValue();
        return super.toString() + " " + val;
    }
}
