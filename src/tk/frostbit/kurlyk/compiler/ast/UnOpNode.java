package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.core.operations.UnaryOperation;

public class UnOpNode extends ExpressionNode {

    public final String opSign;
    public final ExpressionNode expression;

    public UnOpNode(String opSign, AstNode expression) {
        this.opSign = opSign;
        this.expression = expression.asExpression();
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        expression.compile(prog, context);
        final UnaryOperation op = UnaryOperation.bySign(opSign);
        final Type argType = context.types.pop();
        final Type type;
        if (argType.getArity() > 0) {
            throw CompileError.makeNotAppiable(opSign, argType, this);
        } else if (argType != Type.UNDEFINED) {
            // Выполнить проверку типов
            final BaseType resultType = op.typeOfResult(argType.getBase());
            if (resultType == null) {
                throw CompileError.makeNotAppiable(opSign, argType, this);
            }
            type = resultType.asType();
        } else {
            type = Type.UNDEFINED;
        }
        prog.addInstruction(op.getOpcode(), Pcode.VAL_STACK, 0, Pcode.VAL_STACK);
        return type;
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        expression.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        return super.toString() + " " + opSign;
    }

}
