package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class IndexNode extends ExpressionNode {

    public final ExpressionNode expression;
    public final ExpressionNode index;

    public IndexNode(AstNode expression, AstNode index) {
        this.expression = expression.asExpression();
        this.index = index.asExpression();
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        index.compile(prog, context);
        final Type indexType = context.types.pop();
        expression.compile(prog, context);
        final Type exprType = context.types.pop();
        checkTypes(BaseType.INTEGER.asType(), indexType);
        if (exprType != Type.UNDEFINED && exprType.getArity() == 0) {
            throw CompileError.makeIllegalType(exprType, this);
        }
        prog.addInstruction(Opcode.INDEX, Pcode.VAL_STACK, Pcode.VAL_STACK, 0);
        if (exprType == Type.UNDEFINED) {
            return Type.UNDEFINED;
        } else {
            final BaseType baseType = exprType.getBase();
            final int arity = exprType.getArity() - 1;
            return Type.make(baseType, arity);
        }
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        expression.onVisit(visitor, deep + 1);
        index.onVisit(visitor, deep + 1);
    }

}
