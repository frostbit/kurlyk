package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class MemberNode extends ExpressionNode {

    public final String name;
    public final ExpressionNode object;

    public MemberNode(String name, AstNode object) {
        this.name = name;
        this.object = object.asExpression();
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        // Все вызовы обрабатываются в CallNode
        if (object instanceof IdentNode) {
            final String ident = ((IdentNode) object).ident;
            final IClass clazz = prog.getClass(ident);
            if (clazz != null) {
                // Статическое поле
                final Value field = clazz.getStaticField(name);
                if (field == null) {
                    throw CompileError.makeFieldNotFound(name, clazz, this);
                }
                final String globalField = Signature.globalField(clazz, name);
                prog.addInstruction(Opcode.STATFLD, globalField, 0, 0);
                return field.getType();
            }
        }
        // Поле экземпляра объекта
        object.compile(prog, context);
        final Type objType = context.types.pop();
        if ((objType != Type.UNDEFINED && objType.getArity() < 1)
                || !name.equals(Signature.ARRAY_SIZE_FIELD)) {
            checkTypes(BaseType.OBJECT.asType(), objType);
        }
        prog.addInstruction(Opcode.OBJFLD, name, Pcode.VAL_STACK, 0);
        return Type.UNDEFINED;
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        object.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        return super.toString() + " " + name;
    }

}
