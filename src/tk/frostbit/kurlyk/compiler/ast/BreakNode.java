package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Program;

public class BreakNode extends AstNode {
    @Override
    public void compile(Program prog, CompileContext context) {
        prog.addInstruction(Opcode.JMP, 0, 0, 0);
    }
}
