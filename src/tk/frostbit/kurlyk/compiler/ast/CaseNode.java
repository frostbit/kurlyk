package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.Program;
import tk.frostbit.kurlyk.core.Value;

public class CaseNode extends AstNode {

    public final Value constant;
    public final AstNode body;

    public CaseNode(AstNode constant, AstNode body) {
        this.constant = ((ConstNode) constant).value;
        this.body = body;
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        body.compile(prog, context);
    }

    @Override
    public String toString() {
        return super.toString() + " " + constant;
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        body.onVisit(visitor, deep + 1);
    }

}
