package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.*;

public class NewArrayNode extends ExpressionNode {

    public final Type type;
    public final ExpressionNode size;

    public NewArrayNode(Type type, AstNode size) {
        this.type = type;
        this.size = size.asExpression();
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        size.compile(prog, context);
        checkTypes(BaseType.INTEGER.asType(), context.types.pop());
        prog.addInstruction(Opcode.NEWARR, type.getSignature(), Pcode.VAL_STACK, Pcode.VAL_STACK);
        return type;
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        size.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        return super.toString() + " " + type;
    }

}
