package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.*;

public class WhileNode extends AstNode {

    public final ExpressionNode condition;
    public final AstNode body;

    public WhileNode(AstNode condition, AstNode body) {
        this.condition = condition.asExpression();
        this.body = body;
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        final int start = prog.size();
        condition.compile(prog, context);
        checkTypes(BaseType.BOOL.asType(), context.types.pop());
        final int jfAdr = prog.size();
        prog.addInstruction(Opcode.JF, 0, 0, 0);
        body.compile(prog, context);
        prog.addInstruction(Opcode.JMP, start, 0, 0);
        prog.pc(jfAdr).arg1 = prog.size();
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        condition.onVisit(visitor, deep + 1);
        body.onVisit(visitor, deep + 1);
    }

}
