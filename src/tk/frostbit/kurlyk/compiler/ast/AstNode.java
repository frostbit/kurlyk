package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.Program;
import tk.frostbit.kurlyk.core.Type;

public abstract class AstNode {

    public int line;

    AstNode() {

    }

    public final void visit(Visitor visitor) {
        onVisit(visitor, 0);
    }

    public abstract void compile(Program prog, CompileContext context);

    protected void onVisit(Visitor visitor, int deep) {
        visitor.visit(this, deep);
    }

    public interface Visitor {

        void visit(AstNode node, int deep);

    }

    @Override
    public String toString() {
        final String className = getClass().getSimpleName();
        return className.substring(0, className.length() - 4);
    }

    protected final void checkTypes(Type dst, Type src) {
        if (!dst.assignableFrom(src)) {
            throw CompileError.makeTypeMismatch(dst, src, this);
        }
    }

    public final ExpressionNode asExpression() {
        try {
            return (ExpressionNode) this;
        } catch (ClassCastException e) {
            throw CompileError.makeExpressionExpected(this);
        }
    }

}
