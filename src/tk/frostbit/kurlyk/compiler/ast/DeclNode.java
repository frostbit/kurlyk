package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class DeclNode extends AstNode {

    public final String name;
    public final Type type;

    public DeclNode(String name, Type type) {
        this.name = name;
        this.type = type;
        if (type.getBase() == BaseType.VOID) {
            throw CompileError.makeIllegalType(type, this);
        }
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        if (name.equals(Signature.CURRENT_OBJECT_IDENT)) {
            throw new CompileError(
                    "Illegal declaration: " + Signature.CURRENT_OBJECT_IDENT);
        }
        if (context.resolveLocal(name) != 0) {
            throw new CompileError("Variable " + name + " is already defined");
        } else {
            context.addLocal(type, name);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " " + type + " " + name;
    }
}
