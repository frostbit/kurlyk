package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.Program;

import java.util.*;

public class ArgListNode extends AstNode {

    public final List<ExpressionNode> nodes = new ArrayList<>();

    public ArgListNode(List<AstNode> nodes) {
        for (AstNode node : nodes) {
            this.nodes.add(node.asExpression());
        }
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        for (int i = nodes.size() - 1; i >= 0; i--) {
            nodes.get(i).compile(prog, context);
        }
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        for (AstNode node : nodes) {
            node.onVisit(visitor, deep + 1);
        }
    }

}
