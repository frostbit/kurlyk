package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.*;

public class ReturnNode extends AstNode {

    public final ExpressionNode expression;

    public ReturnNode(AstNode expression) {
        if (expression != null) {
            this.expression = expression.asExpression();
        } else {
            this.expression = null;
        }
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        if (expression != null) {
            expression.onVisit(visitor, deep + 1);
        }
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        final Pcode pcode = new Pcode(Opcode.RET);
        final Type returnType = context.method.getType();
        if (expression == null) {
            checkTypes(returnType, BaseType.VOID.asType());
        } else {
            expression.compile(prog, context);
            pcode.arg1 = Pcode.VAL_STACK;
            checkTypes(returnType, context.types.pop());
        }
        prog.addInstruction(pcode);
    }
}
