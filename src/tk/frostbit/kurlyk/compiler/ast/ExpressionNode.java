package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.core.*;

public abstract class ExpressionNode extends AstNode {

    @Override
    public final void compile(Program prog, CompileContext context) {
        final Type type = compileExpression(prog, context);
        context.types.push(type);
    }

    // TODO: make abstract
    protected abstract Type compileExpression(Program prog, CompileContext context);

}
