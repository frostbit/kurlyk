package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.core.operations.BinaryOperation;

public class BinOpNode extends ExpressionNode {

    public final ExpressionNode left;
    public final ExpressionNode right;
    public final String opSign;

    public BinOpNode(String op, AstNode left, AstNode right) {
        this.opSign = op;
        this.left = left.asExpression();
        this.right = right.asExpression();
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        right.compile(prog, context);
        left.compile(prog, context);
        final BinaryOperation op = BinaryOperation.bySign(opSign);
        final Type leftType = context.types.pop();
        final Type rightType = context.types.pop();
        final Type type;
        if (leftType.getArity() > 0 || rightType.getArity() > 0) {
            throw CompileError.makeNotAppiable(opSign, leftType, rightType, this);
        } else if (leftType != Type.UNDEFINED && rightType != Type.UNDEFINED) {
            final BaseType resultType = op.typeOfResult(leftType.getBase(), rightType.getBase());
            if (resultType == null) {
                throw CompileError.makeNotAppiable(opSign, leftType, rightType, this);
            }
            type = resultType.asType();
        } else {
            type = Type.UNDEFINED;
        }
        prog.addInstruction(op.getOpcode(), Pcode.VAL_STACK, Pcode.VAL_STACK, Pcode.VAL_STACK);
        return type;
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        left.onVisit(visitor, deep + 1);
        right.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        return super.toString() + " " + opSign;
    }
}
