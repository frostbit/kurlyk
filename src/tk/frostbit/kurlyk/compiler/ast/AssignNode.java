package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class AssignNode extends AstNode {

    public final ExpressionNode lvalue;
    public final ExpressionNode rvalue;

    public AssignNode(AstNode lvalue, AstNode rvalue) {
        if (lvalue instanceof CallNode) {
            throw CompileError.makeAssignmentToCall((CallNode) lvalue);
        }
        this.lvalue = lvalue.asExpression();
        this.rvalue = rvalue.asExpression();
    }

    @Override
    public void compile(Program prog, CompileContext context) {
        // Оптимизация для присвоений локальным переменным
        if (lvalue instanceof IdentNode) {
            final IdentNode lid = (IdentNode) lvalue;
            final int local = context.resolveLocal(lid.ident);
            if (local != 0) {
                final Type localType = context.localType(local);
                rvalue.compile(prog, context);
                final Type rtype = context.types.pop();
                checkTypes(localType, rtype);
                prog.addInstruction(Opcode.MOV, Pcode.VAL_STACK, 0, local);
                return;
            }
        }
        lvalue.compile(prog, context);
        final Type ltype = context.types.pop();
        rvalue.compile(prog, context);
        final Type rtype = context.types.pop();
        checkTypes(ltype, rtype);
        prog.addInstruction(Opcode.ASSIGN, Pcode.VAL_STACK, 0, 0);
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        lvalue.onVisit(visitor, deep + 1);
        rvalue.onVisit(visitor, deep + 1);
    }
}
