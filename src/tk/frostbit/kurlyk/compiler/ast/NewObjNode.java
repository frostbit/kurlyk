package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class NewObjNode extends ExpressionNode {

    public final String className;
    public final ArgListNode ctorArgs;

    public NewObjNode(String className, AstNode ctorArgs) {
        this.className = className;
        this.ctorArgs = (ArgListNode) ctorArgs;
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        ctorArgs.compile(prog, context);
        final int argc = ctorArgs.nodes.size();
        final String ctorSignature = Signature.method(Signature.CTOR_NAME, argc);
        final IClass clazz = prog.getClass(className);
        if (clazz.getMethod(ctorSignature) == null) {
            throw CompileError.makeMethodNotFound(ctorSignature, clazz, this);
        }
        prog.addInstruction(Opcode.NEWOBJ, className, argc, Pcode.VAL_STACK);
        for (int i = 0; i < argc; i++) {
            // Снятие со стека типов аргументов
            context.types.pop();
        }
        return BaseType.OBJECT.asType();
    }

    @Override
    protected void onVisit(Visitor visitor, int deep) {
        super.onVisit(visitor, deep);
        ctorArgs.onVisit(visitor, deep + 1);
    }

    @Override
    public String toString() {
        return super.toString() + " " + className;
    }

}
