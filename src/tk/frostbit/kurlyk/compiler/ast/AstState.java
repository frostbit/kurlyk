package tk.frostbit.kurlyk.compiler.ast;

import java.util.*;

public class AstState {

    public final List<AstNode> nodes = new ArrayList<>();

    public AstNode popNode() {
        final int index = nodes.size() - 1;
        final AstNode node = nodes.get(index);
        nodes.remove(index);
        return node;
    }

    public void pushNode(AstNode node) {
        nodes.add(node);
    }

}
