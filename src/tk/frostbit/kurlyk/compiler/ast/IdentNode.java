package tk.frostbit.kurlyk.compiler.ast;

import tk.frostbit.kurlyk.compiler.CompileContext;
import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.core.*;

public class IdentNode extends ExpressionNode {

    public final String ident;

    public IdentNode(String ident) {
        this.ident = ident;
    }

    @Override
    public String toString() {
        return super.toString() + " " + ident;
    }

    @Override
    protected Type compileExpression(Program prog, CompileContext context) {
        // Только локальные переменные и поля,
        // классы должны обрабатываться в других местах (Call, Member)
        final int local = context.resolveLocal(ident);
        if (local != 0) {
            // Локальная переменная
            prog.addInstruction(Opcode.MOV, local, 0, Pcode.VAL_STACK);
            return context.localType(local);
        }
        final Type instField = context.findInstanceField(ident);
        if (instField != null) {
            if (context.method.isStatic()) {
                throw CompileError.makeStaticInstAccess(ident, this);
            } else {
                // Поле текущего объекта
                prog.addInstruction(Opcode.OBJFLD, ident, 1, 0);
                return instField;
            }
        }
        final Type statField = context.findStaticField(ident);
        if (statField != null) {
            // Статическое поле текущего класса
            final String signature = Signature.globalField(context.clazz, ident);
            prog.addInstruction(Opcode.STATFLD, signature, 0, 0);
            return statField;
        }
        throw CompileError.makeUnknownIdentifier(this);
    }

}
