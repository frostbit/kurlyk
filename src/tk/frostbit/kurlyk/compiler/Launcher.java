package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.Io;
import tk.frostbit.kurlyk.StringUtils;
import tk.frostbit.kurlyk.compiler.ast.AstNode;
import tk.frostbit.kurlyk.core.Program;
import tk.frostbit.kurlyk.runtime.Interpreter;

import java.io.File;
import java.util.*;

import static java.lang.System.out;

public class Launcher {

    public static void main(String[] args) throws Exception {
        boolean printAst = false;
        boolean printDump = false;
        boolean printTrace = false;
        for (String arg : args) {
            switch (arg) {
                case "-a": printAst = true; break;
                case "-d": printDump = true; break;
                case "-t": printTrace = true; break;
            }
        }

        final File srcFile = new File(args[0]);
        final File srcRoot = srcFile.getParentFile();
        final SyntaxCallback sc = SyntaxCallback.getInstance();

        final Compiler compiler = new Compiler();
        final Set<String> parsedClasses = new HashSet<>();
        final List<String> dependencies = new ArrayList<>();
        final List<ClassDef> classDefs = new ArrayList<>();
        Lexem syntResult = null;
        CompileError err = null;
        Program program = null;
        final String mainClassName = getNameWoExt(srcFile);
        dependencies.add(mainClassName);
        try {
            while (!dependencies.isEmpty()) {
                final String className = dependencies.remove(0);
                out.println("Parsing " + className);
                sc.setSourceFileName(className);
                final String input = Io.readFile(
                        new File(srcRoot, className + ".kur").getAbsolutePath());
                final SyntaxAnalyzer sa = new SyntaxAnalyzer(input);
                syntResult = sa.parse();
                if (syntResult == null) {
                    final ClassDef classDef = SyntaxCallback.getInstance().getDefinedClass();
                    classDefs.add(classDef);
                    compiler.loadClass(classDef);
                    parsedClasses.add(className);
                    sc.popDependencies(dependencies, parsedClasses);
                } else {
                    break;
                }
            }
            if (syntResult == null) {
                out.println("Compiling...");
                compiler.compileClasses();
                program = compiler.getProgram();
            }
        } catch (CompileError e) {
            err = e;
        }
        if (syntResult != null) {
            out.printf("Syntax error: illegal lexem \"%s\" at line %d\n",
                    syntResult.text, syntResult.line);
        } else if (err != null) {
            out.println("Compilation error: " + err.getMessage());
            err.printStackTrace(out);
        } else {
            out.println("Compiled successfully\n");
            if (printAst) {
                for (ClassDef def : classDefs) {
                    printAst(def);
                }
            }
            if (printDump) {
                program.dump(out);
            }
            out.println("Run\n");
            final Interpreter interpreter = new Interpreter(program);
            interpreter.setTraceEnabled(printTrace);
            try {
                interpreter.call(mainClassName + ".main");
                if (printTrace) {
                    interpreter.printTrace(out);
                }
                interpreter.printStats(out);
            } catch (Exception e) {
                interpreter.printTrace(out);
                e.printStackTrace(out);
            }
        }
    }

    private static String getNameWoExt(File file) {
        final String name = file.getName();
        return name.substring(0, name.lastIndexOf('.'));
    }

    private static void printAst(ClassDef clazz) {
        out.println("Class " + clazz.getName());
        for (FieldDef field : clazz.getFields()) {
            out.println("  F " + field);
        }
        for (MethodDef method : clazz.getMethods()) {
            out.println("  M " + method);
            for (String arg : method.getArgNames()) {
                out.println("    " + "Argument " + arg);
            }
            if (method.getAstRoot() != null) {
                method.getAstRoot().visit(new AstNode.Visitor() {
                    @Override
                    public void visit(AstNode node, int deep) {
                        out.print(StringUtils.repeat("    ", "  ", deep));
                        out.println(node);
                    }
                });
            }
        }
    }

}
