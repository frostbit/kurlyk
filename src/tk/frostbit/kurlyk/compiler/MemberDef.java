package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.StringUtils;
import tk.frostbit.kurlyk.core.Signature;
import tk.frostbit.kurlyk.core.Type;

import java.util.*;

public abstract class MemberDef {

    private final String name;
    private final String signature;
    private final boolean isStatic;
    private final Type type;
    private final List<Type> argTypes;

    MemberDef(String name, Type type, List<Type> argTypes, boolean isStatic) {
        this.name = name;
        this.signature = Signature.method(name, argTypes);
        this.type = type;
        this.argTypes = argTypes;
        this.isStatic = isStatic;
    }

    public String getName() {
        return name;
    }

    public String getSignature() {
        return signature;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public Type getType() {
        return type;
    }

    public List<Type> getArgTypes() {
        return argTypes;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(isStatic ? "static" : "instance").append(" ");
        sb.append(type).append(" ").append(name);
        if (argTypes != null) {
            sb.append("(");
            StringUtils.join(sb, ", ", argTypes);
            sb.append(")");
        }
        return sb.toString();
    }

}
