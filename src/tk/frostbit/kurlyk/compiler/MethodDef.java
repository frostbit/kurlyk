package tk.frostbit.kurlyk.compiler;

import tk.frostbit.kurlyk.compiler.ast.BlockNode;
import tk.frostbit.kurlyk.core.Type;

import java.util.*;

public class MethodDef extends MemberDef {

    private List<String> argNames;
    private BlockNode astRoot;

    public MethodDef(String name, Type type, List<Type> argTypes,
                     List<String> argNames, boolean isStatic) {
        super(name, type, argTypes, isStatic);
        this.argNames = argNames;
    }

    public List<String> getArgNames() {
        return argNames;
    }

    public BlockNode getAstRoot() {
        return astRoot;
    }

    public void setAstRoot(BlockNode astRoot) {
        this.astRoot = astRoot;
    }

}
