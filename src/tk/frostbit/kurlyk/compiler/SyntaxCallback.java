package tk.frostbit.kurlyk.compiler;

import java.util.*;

import tk.frostbit.kurlyk.compiler.ast.*;
import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Signature;
import tk.frostbit.kurlyk.core.Type;

/**
 * Класс, который обрабатывает действия в грамматике языка.
 * Методы вызываются из построенного в WTL анализатора.
 */
class SyntaxCallback implements ISyntaxCallback {

    private static SyntaxCallback instance = new SyntaxCallback();

    private Stack<Lexem> stack = new Stack<>();
    private Stack<AstState> astStates = new Stack<>();
    private Stack<Type> types = new Stack<>();
    private SyntaxState state = new SyntaxState();
    private List<String> dependencies = new ArrayList<>();
    private ClassDef classDef;
    private MethodDef methodDef;
    private String sourceFileName;

    private SyntaxCallback() { }

    static SyntaxCallback getInstance() {
        return instance;
    }

    ClassDef getDefinedClass() {
        return classDef;
    }

    public void setSourceFileName(String name) {
        sourceFileName = name;
    }

    public void popDependencies(Collection<String> dest, Collection<String> exclude) {
        for (String file : dependencies) {
            if (!dest.contains(file)) {
                if (exclude == null || !exclude.contains(file)) {
                    dest.add(file);
                }
            }
        }
        dependencies.clear();
    }

    private AstState ast() {
        return astStates.empty() ? null : astStates.peek();
    }

    private void allocateAstState() {
        astStates.push(new AstState());
    }

    private void popAstState() {
        astStates.pop();
    }

    @Override
    public void onImport(Lexem lexem) {
        dependencies.add(lexem.text.toString());
    }

    @Override
    public void onClassDef(Lexem lexem) {
        final String className = lexem.text.toString();
        if (!className.equals(sourceFileName)) {
            throw new CompileError(
                    "Class " + className + " must be placed in file " + className + ".kur");
        }
        classDef = new ClassDef(className);
        allocateAstState();
    }

    @Override
    public void onClassCommit(Lexem lexem) {
        boolean hasCtor = false;
        for (MethodDef method : classDef.getMethods()) {
            if (method.getName().equals(Signature.CTOR_NAME)) {
                hasCtor = true;
                break;
            }
        }
        if (!hasCtor) {
            final MethodDef defaultCtor = new MethodDef(
                    Signature.CTOR_NAME,
                    BaseType.VOID.asType(),
                    Collections.<Type>emptyList(),
                    Collections.<String>emptyList(),
                    false);
            defaultCtor.setAstRoot(new BlockNode(Collections.<AstNode>emptyList()));
            classDef.addMethod(defaultCtor);
        }
        popAstState();
    }

    @Override
    public void onIdent(Lexem lexem) {
        ast().pushNode(new IdentNode(lexem.text.toString()));
    }

    @Override
    public void onMemberDef(Lexem lexem) {
        state.modStatic = false;
        state.argTypes = null;
    }

    @Override
    public void onInit(Lexem lexem) {
        final String fieldName = state.entityName;
        final AstNode value = ast().popNode();
        final MethodDef initializer = state.modStatic ?
                classDef.getStaticInitializer() : classDef.getInitializer();
        final AssignNode assignNode = new AssignNode(new IdentNode(fieldName), value);
        initializer.getAstRoot().nodes.add(assignNode);
    }

    @Override
    public void onAssign(Lexem lexem) {
        final AstNode rvalue = ast().popNode();
        final AstNode lvalue = ast().popNode();
        final AssignNode node = new AssignNode(lvalue, rvalue);
        ast().pushNode(node);
    }

    @Override
    public void onDecl(Lexem lexem) {
        state.entityName = null;
    }

    @Override
    public void onFieldCommit(Lexem lexem) {
        final FieldDef field = new FieldDef(
                state.entityName,
                types.pop(),
                state.modStatic
        );
        classDef.addField(field);
    }

    @Override
    public void onLocalDef(Lexem lexem) {
        final String name = state.entityName;
        final Type type = types.pop();
        final DeclNode node = new DeclNode(name, type);
        ast().pushNode(node);
    }

    @Override
    public void onInitLocal(Lexem lexem) {
        final String name = state.entityName;
        final AstNode value = ast().popNode();
        final AssignNode node = new AssignNode(new IdentNode(name), value);
        ast().pushNode(node);
    }

    @Override
    public void onStatic(Lexem lexem) {
        state.modStatic = true;
    }

    @Override
    public void onType(Lexem lexem) {
        state.arrayArity = 0;
        state.baseType = BaseType.byName(lexem.text.toString());
    }

    @Override
    public void onArrayType(Lexem lexem) {
        state.arrayArity++;
    }

    @Override
    public void onTypeCommit(Lexem lexem) {
        final Type type = Type.make(state.baseType, state.arrayArity);
        types.push(type);
    }

    @Override
    public void onEntityName(Lexem lexem) {
        state.entityName = lexem.text.toString();
    }

    @Override
    public void onCtor(Lexem lexem) {
        state.entityName = Signature.CTOR_NAME;
        types.push(Type.base(BaseType.VOID));
        state.modStatic = false;
    }

    @Override
    public void onArgsDef(Lexem lexem) {
        state.argTypes = new ArrayList<>();
        state.methArgs = new ArrayList<>();
    }

    @Override
    public void onArgDef(Lexem lexem) {
        final String argName = lexem.text.toString();
        final Type argType = types.pop();
        if (argType.getBase() == BaseType.VOID) {
            throw new CompileError("Illegal argument type " + argType + " at line " + lexem.line);
        }
        state.methArgs.add(argName);
        state.argTypes.add(argType);
    }

    @Override
    public void onMethStart(Lexem lexem) {
        final MethodDef method = new MethodDef(
                state.entityName,
                types.pop(),
                state.argTypes,
                state.methArgs,
                state.modStatic
        );
        classDef.addMethod(method);
        methodDef = method;
    }

    @Override
    public void onMethCommit(Lexem lexem) {
        final BlockNode astRoot = (BlockNode) ast().popNode();
        methodDef.setAstRoot(astRoot);
        methodDef = null;
    }

    @Override
    public void onBlock(Lexem lexem) {
        allocateAstState();
    }

    @Override
    public void onBlockCommit(Lexem lexem) {
        final BlockNode node = new BlockNode(ast().nodes);
        popAstState();
        ast().pushNode(node);
    }

    @Override
    public void onMember(Lexem lexem) {
        final String name = lexem.text.toString();
        final AstNode obj = ast().popNode();
        final MemberNode node = new MemberNode(name, obj);
        ast().pushNode(node);
    }

    @Override
    public void onIndex(Lexem lexem) {
        final AstNode index = ast().popNode();
        final AstNode exp = ast().popNode();
        final IndexNode node = new IndexNode(exp, index);
        ast().pushNode(node);
    }

    @Override
    public void onNewObj(Lexem lexem) {
        stack.push(lexem);
    }

    @Override
    public void onCtorCall(Lexem lexem) {
        final String className = stack.pop().text.toString();
        final AstNode ctorArgs = ast().popNode();
        final NewObjNode node = new NewObjNode(className, ctorArgs);
        ast().pushNode(node);
    }

    @Override
    public void onNewArray(Lexem lexem) {
        final Type type = types.pop();
        final AstNode sizeExp = ast().popNode();
        final NewArrayNode node = new NewArrayNode(type, sizeExp);
        ast().pushNode(node);
    }

    @Override
    public void onArgs(Lexem lexem) {
        allocateAstState();
    }

    @Override
    public void onArgsCommit(Lexem lexem) {
        final ArgListNode node = new ArgListNode(ast().nodes);
        popAstState();
        ast().pushNode(node);
    }

    @Override
    public void onCall(Lexem lexem) {
        final AstState ast = ast();
        final AstNode args = ast.popNode();
        final AstNode obj = ast.popNode();
        ast.pushNode(new CallNode(obj, args));
    }

    @Override
    public void onUnOp(Lexem lexem) {
        stack.push(lexem);
    }

    @Override
    public void onCommitUn(Lexem lexem) {
        final String op = stack.pop().text.toString();
        final AstNode expr = ast().popNode();
        final UnOpNode node = new UnOpNode(op, expr);
        ast().pushNode(node);
    }

    @Override
    public void onConst(Lexem lexem) {
        ast().pushNode(new ConstNode(lexem.text.toString()));
    }

    @Override
    public void onBinOp(Lexem lexem) {
        stack.push(lexem);
    }

    @Override
    public void onCommitBin(Lexem lexem) {
        final String op = stack.pop().text.toString();
        final AstNode right = ast().popNode();
        final AstNode left = ast().popNode();
        final BinOpNode node = new BinOpNode(op, left, right);
        ast().pushNode(node);
    }

    @Override
    public void onReturnExp(Lexem lexem) {
        final AstNode expression = ast().popNode();
        final ReturnNode node = new ReturnNode(expression);
        ast().pushNode(node);
    }

    @Override
    public void onReturnVoid(Lexem lexem) {
        final ReturnNode node = new ReturnNode(null);
        ast().pushNode(node);
    }

    @Override
    public void onIf(Lexem lexem) {
        ast().pushNode(null);
        onIfElse(lexem);
    }

    @Override
    public void onIfElse(Lexem lexem) {
        final AstNode elseBody = ast().popNode();
        final AstNode body = ast().popNode();
        final AstNode condition = ast().popNode();
        final IfNode node = new IfNode(condition, body, elseBody);
        ast().pushNode(node);
    }

    @Override
    public void onWhile(Lexem lexem) {
        final AstNode body = ast().popNode();
        final AstNode condition = ast().popNode();
        final WhileNode node = new WhileNode(condition, body);
        ast().pushNode(node);
    }

    @Override
    public void onBreak(Lexem lexem) {
        ast().pushNode(new BreakNode());
    }

    @Override
    public void onSwitch(Lexem lexem) {
        allocateAstState();
    }

    @Override
    public void onCase(Lexem lexem) {
        final AstNode body = ast().popNode();
        final AstNode constant = ast().popNode();
        final CaseNode node = new CaseNode(constant, body);
        ast().pushNode(node);
    }

    @Override
    public void onSwitchCommit(Lexem lexem) {
        final List<AstNode> nodes = ast().nodes;
        popAstState();
        final AstNode expression = ast().popNode();
        final SwitchNode node = new SwitchNode(expression, nodes);
        ast().pushNode(node);
    }
}
