package tk.frostbit.kurlyk.runtime;

import tk.frostbit.kurlyk.core.Value;

public class StackFrame {

    public Value<?>[] locals;

    int returnAddress;
    int lastStackSize;
    int retValDest;

}
