package tk.frostbit.kurlyk.runtime;

import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.core.operations.BinaryOperation;
import tk.frostbit.kurlyk.core.operations.UnaryOperation;

import java.io.PrintStream;
import java.util.*;

public class Interpreter {

    private final Program prog;
    private final Stack<StackFrame> frames = new Stack<>();
    private final Stack<Value<?>> stack = new Stack<>();
    private final List<Integer> trace = new ArrayList<>();

    private int ip = -1;
    private boolean traceEnabled;

    private long totalCycles = 0;
    private long stackOperations = 0;
    private long totalTime;

    public Interpreter(Program prog) {
        this.prog = prog;
    }

    public Value call(String methodName, Value<?>... args) {
        initClasses();
        final long startTime = System.currentTimeMillis();
        final String signature = Signature.method(methodName, args.length);
        final IMethod method = resolveMethod(signature, null);
        if (method == null) {
            throw new RuntimeError("Method " + signature + " is not found");
        }
        if (!method.isStatic()) {
            throw new IllegalArgumentException();
        } else {
            final List<Value<?>> argList = new ArrayList<>();
            Collections.addAll(argList, args);
            Collections.reverse(argList);
            stack.addAll(argList);
            if (method.isNative()) {
                return method.callNative(args.length, stack);
            }
            final StackFrame frame = new StackFrame();
            frame.returnAddress = ip;
            frame.retValDest = Pcode.VAL_STACK;
            ip = method.call(args.length, stack, frame);
            frames.push(frame);
            run();
        }
        totalTime += System.currentTimeMillis() - startTime;
        return stack.pop();
    }

    private void initClasses() {
        final String statInitSignature = Signature.method(Signature.STATIC_INIT_NAME, 0);
        IClass clazz;
        while ((clazz = prog.nextUninitialized()) != null) {
            final IMethod staticInitializer = clazz.getMethod(statInitSignature);
            execute(staticInitializer, 0, 0);
        }
    }

    public void setTraceEnabled(boolean traceEnabled) {
        this.traceEnabled = traceEnabled;
    }

    public void printTrace(PrintStream out) {
        for (int ip : trace) {
            out.printf("TRACE: %3d %s\n", ip, prog.pc(ip).dasm(prog));
        }
    }

    public void printStats(PrintStream out) {
        out.println("Total cycles: " + totalCycles);
        out.println("Total stack operations: " + stackOperations);
        out.println("Total time: " + totalTime);
    }

    private void run() {
        while (ip != -1) {
            totalCycles++;
            final Pcode pc = prog.pc(ip);
            final Opcode opcode = pc.op;
            final int arg1 = pc.arg1;
            final int arg2 = pc.arg2;
            final int dest = pc.dest;
            if (traceEnabled) {
                trace.add(ip);
            }
            switch (opcode) {
                case MOV: {
                    setValue(dest, getValue(arg1));
                    break;
                }
                case ASSIGN: {
                    final Value<?> value = getValue(arg1);
                    stack.pop().assign(value);
                    break;
                }
                case CALL: {
                    final String signature = prog.getSymbol(arg1);
                    final IMethod method = resolveMethod(signature, null);
                    if (method == null) {
                        throw new RuntimeError("Method " + signature + " is not found");
                    }
                    call(method, arg2, dest);
                    break;
                }
                case CALLVIRT: {
                    final String signature = prog.getSymbol(arg1);
                    final Value<?> value = stack.pop();
                    final IObject obj = value.cast(BaseType.OBJECT);
                    final IClass clazz = obj.clazz();
                    final IMethod method = resolveMethod(signature, clazz);
                    if (method == null) {
                        throw new RuntimeError(
                                "Method " + signature + " is not found in class " + clazz);
                    }
                    // Возврат объекта в стек для вызова метода
                    stack.push(value);
                    call(method, arg2, dest);
                    break;
                }
                case NEWOBJ: {
                    final String className = prog.getSymbol(arg1);
                    final IClass clazz = prog.getClass(className);
                    final Value obj = clazz.newInstance();
                    if (obj == null) {
                        throw new RuntimeError("Class " + className + " is not instantiable");
                    }
                    final IMethod init = resolveMethod(Signature.INIT_NAME, clazz);
                    final String ctorSignature = Signature.method(Signature.CTOR_NAME, arg2);
                    final IMethod ctor = resolveMethod(ctorSignature, clazz);
                    stack.push(obj);
                    execute(init, 0, 0);
                    stack.push(obj);
                    execute(ctor, arg2, 0);
                    setValue(dest, obj);
                    break;
                }
                case STATFLD: {
                    final String signature = prog.getSymbol(arg1);
                    final String className = Signature.extractClass(signature);
                    final String fieldName = Signature.woClassName(signature);
                    final IClass clazz = prog.getClass(className);
                    final Value field = clazz.getStaticField(fieldName);
                    if (field == null) {
                        throw new RuntimeError("Field " + fieldName + " not found in " + clazz);
                    }
                    stack.push(field);
                    break;
                }
                case OBJFLD: {
                    final String fieldName = prog.getSymbol(arg1);
                    final Value<?> value = getValue(arg2);
                    final Value field;
                    if (value.getType().getArity() > 0
                            && fieldName.equals(Signature.ARRAY_SIZE_FIELD)) {
                        field = new Value<>(BaseType.INTEGER, value.arraySize());
                    } else {
                        final IObject obj = value.cast(BaseType.OBJECT);
                        field = obj.getField(fieldName);
                        if (field == null) {
                            throw new RuntimeError("Field " + fieldName + " not found in " + obj);
                        }
                    }
                    stack.push(field);
                    break;
                }
                case NEWARR: {
                    final String typeSignature = prog.getSymbol(arg1);
                    final Type<?> type = Type.fromSignature(typeSignature);
                    final Value<?> sizeVal = getValue(arg2);
                    final int size = sizeVal.cast(BaseType.INTEGER);
                    if (dest == Pcode.VAL_STACK) {
                        final Value arrValue = new Value<>(type);
                        arrValue.newArray(size);
                        stackOperations++;
                        stack.push(arrValue);
                    } else {
                        final Value arrValue = getValue(dest);
                        arrValue.newArray(size);
                    }
                    break;
                }
                case INDEX: {
                    final Value<?> array = getValue(arg1);
                    final Value<?> vIndex = getValue(arg2);
                    final int index = vIndex.cast(BaseType.INTEGER);
                    final Value element = array.getElement(index);
                    stack.push(element);
                    break;
                }
                case JMP: {
                    ip = arg1;
                    continue;
                }
                case JF: {
                    final Value condition = stack.pop();
                    if (condition.cast(BaseType.BOOL).equals(Boolean.FALSE)) {
                        ip = arg1;
                        continue;
                    }
                    break;
                }
                case JE: {
                    final Value left = stack.peek();
                    final Value right = getValue(arg2);
                    if (left.getPrimValue().equals(right.cast(left.getType()))) {
                        ip = arg1;
                        continue;
                    }
                    break;
                }
                case RET: {
                    final Value retVal = getValue(arg1);
                    if (stack.size() < sf().lastStackSize) {
                        throw new RuntimeError("Stack corruption detected");
                    }
                    // Очистка стека от мусора
                    while (stack.size() > sf().lastStackSize) {
                        stack.pop();
                    }
                    ip = sf().returnAddress;
                    int retValDest = sf().retValDest;
                    frames.pop();
                    setValue(retValDest, retVal);
                    if (ip == -1) {
                        // Выполнение завершено, результат на стеке
                        return;
                    }
                    break;
                }
                case NEG:
                case INV: {
                    final UnaryOperation op = UnaryOperation.byOpcode(opcode);
                    final Value<?> arg = getValue(arg1);
                    final Type<?> argType = arg.getType();
                    final BaseType resultType = op.typeOfResult(argType.getBase());
                    if (argType.getArity() > 0 || resultType == null) {
                        throw new RuntimeError(
                                "Cannot apply \"" + op.getSign() + "\" to " + argType);
                    }
                    final Value<?> result;
                    if (dest == Pcode.VAL_STACK) {
                        result = resultType.asType().newValue();
                        stackOperations++;
                        stack.push(result);
                    } else if (dest > 0) {
                        result = getValue(arg1);
                    } else {
                        throw new RuntimeError("Illegal assignment");
                    }
                    op.apply(arg, result);
                    break;
                }
                case ADD:
                case SUB:
                case MUL:
                case DIV:
                case MOD:
                case CEQ:
                case CNE:
                case CGE:
                case CLE:
                case CL:
                case CG:
                case AND:
                case OR: {
                    final BinaryOperation op = BinaryOperation.byOpcode(opcode);
                    final Value<?> left = getValue(arg1);
                    final Value<?> right = getValue(arg2);
                    final Type<?> leftType = left.getType();
                    final Type<?> rightType = right.getType();
                    final BaseType resultType = op.typeOfResult(
                            leftType.getBase(), rightType.getBase());
                    if (leftType.getArity() > 0 || rightType.getArity() > 0
                            || resultType == null) {
                        throw new RuntimeError("Cannot apply \"" + op.getSign()
                                + "\" to " + leftType + " and " + rightType);
                    }
                    final Value<?> result;
                    if (dest == Pcode.VAL_STACK) {
                        result = resultType.asType().newValue();
                        stackOperations++;
                        stack.push(result);
                    } else if (dest > 0) {
                        result = getValue(dest);
                    } else {
                        throw new RuntimeError("Illegal assignment");
                    }
                    op.apply(left, right, result);
                    break;
                }
                case SOURCE:
                case NOP:
                    break;
                default:
                    throw new RuntimeError("Unknown opcode: " + opcode);
            }
            ip++;
        }
    }

    private void execute(IMethod method, int argc, int dest) {
        if (method.isNative()) {
            // Нативный метод будет выполнен полностью при вызове
            call(method, argc, dest);
        } else {
            final int returnAddress = ip;
            // Вызов метода с возвратом из run()
            ip = -1;
            call(method, argc, dest);
            ip++;
            // Выполнение до возврата в -1
            run();
            // Восстановление ip
            ip = returnAddress;
        }
    }

    private void call(IMethod method, int argc, int dest) {
        if (method.isNative()) {
            final Value result = method.callNative(argc, stack);
            setValue(dest, result);
        } else {
            final StackFrame frame = new StackFrame();
            frame.returnAddress = ip;
            frame.retValDest = dest;
            ip = method.call(argc, stack, frame) - 1;
            frame.lastStackSize = stack.size();
            frames.push(frame);
        }
    }

    private IMethod resolveMethod(String signature, IClass clazz) {
        if (clazz == null) {
            final String className = Signature.extractClass(signature);
            clazz = prog.getClass(className);
            signature = Signature.woClassName(signature);
        }
        return clazz.getMethod(signature);
    }

    private StackFrame sf() {
        return frames.peek();
    }

    private Value<?> getValue(int id) {
        if (id < Pcode.VAL_STACK) {
            return prog.getConstant(id);
        } else if (id == Pcode.VAL_STACK) {
            stackOperations++;
            return stack.pop();
        } else if (id == 0) {
            return Value.VOID;
        } else {
            return sf().locals[id - 1];
        }
    }

    private void setValue(int id, Value<?> val) {
        if (id < Pcode.VAL_STACK) {
            throw new RuntimeError("Cannot assign to constant");
        } else if (id == Pcode.VAL_STACK) {
            stackOperations++;
            stack.push(val);
        } else if (id > 0) {
            sf().locals[id - 1].assign(val);
        }
    }

}
