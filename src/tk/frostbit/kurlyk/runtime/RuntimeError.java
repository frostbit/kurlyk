package tk.frostbit.kurlyk.runtime;

public class RuntimeError extends RuntimeException {

    public RuntimeError(String message) {
        super(message);
    }

}
