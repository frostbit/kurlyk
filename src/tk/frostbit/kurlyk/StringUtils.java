package tk.frostbit.kurlyk;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    private StringUtils() {
        throw new UnsupportedOperationException();
    }

    public static String repeat(String prefix, String str, int count) {
        if (count == 0) {
            return prefix;
        }
        final StringBuilder sb = new StringBuilder(prefix);
        for (int i = 0; i < count; i++) {
            sb.append(str);
        }
        return sb.toString();
    }

    public static void join(StringBuilder out, String delimiter, Iterable<?> source) {
        boolean first = true;
        for (Object obj : source) {
            if (!first) {
                out.append(delimiter);
            }
            out.append(obj);
            first = false;
        }
    }

    public static void cut(StringBuilder sb, String startMarker, String endMarker) {
        final int startPos = sb.indexOf(startMarker);
        final int endPos = sb.indexOf(endMarker) + endMarker.length();
        sb.delete(startPos, endPos);
    }


    public static void insertAfter(StringBuilder sb, String search, String insert) {
        final int markerPos = sb.indexOf(search);
        if (markerPos == -1) {
            throw new IllegalArgumentException();
        }
        final int pos = markerPos + search.length();
        sb.insert(pos, insert);
    }

    public static void replaceAll(StringBuilder sb, int pos, String search, String replace) {
        while ((pos = sb.indexOf(search, pos)) != -1) {
            sb.replace(pos, pos + search.length(), replace);
            pos += replace.length();
        }
    }

    /**
     * Заменяет одно слово на другое.
     *
     * @param src исходное слово (\w+)
     * @param dst подставляемое слово (\w+)
     */
    public static void replaceWord(StringBuilder sb, String src, String dst) {
        if (!src.matches("\\w+") || !dst.matches("\\w+")) {
            throw new IllegalArgumentException();
        }

        // Easy-breezy implementation using java.util.regex
        // So inefficient
        final Pattern pattern = Pattern.compile("(\\W)" + src + "(\\W)");
        final Matcher matcher = pattern.matcher(sb);
        final String result = matcher.replaceAll("$1" + dst + "$2");
        sb.setLength(0);
        sb.append(result);

    }

}
