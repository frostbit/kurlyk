package tk.frostbit.kurlyk.core;

import tk.frostbit.kurlyk.compiler.CompileError;
import tk.frostbit.kurlyk.StringUtils;

import java.util.*;

public class Type<T> {

    public static final Type UNDEFINED = new Type();

    private static final Map<String, Type> cache = new HashMap<>();

    private final int arity;
    private final BaseType<T> baseType;
    private final String signature;

    private Type() {
        this.arity = 0;
        this.baseType = null;
        this.signature = "undefined";
    }

    private Type(int arity, BaseType<T> baseType) {
        this.arity = arity;
        this.baseType = baseType;
        this.signature = getSignature(baseType, arity);
        if (baseType == BaseType.VOID && arity > 0) {
            throw new CompileError("Illegal type: " + signature);
        }
    }

    public static <T> Type<T> base(BaseType<T> baseType) {
        return make(baseType, 0);
    }

    public static <T> Type<T> make(BaseType<T> baseType, int arity) {
        final String signature = getSignature(baseType, arity);
        if (!cache.containsKey(signature)) {
            final Type<T> type = new Type<>(arity, baseType);
            cache.put(signature, type);
            return type;
        }
        // Type safety is guaranteed by signature
        @SuppressWarnings("unchecked")
        final Type<T> type = cache.get(signature);
        return type;
    }

    public Value<T> newValue() {
        final Value<T> value = new Value<>(this);
        if (arity == 0 && baseType != null) {
            value.assign(baseType.defaultValue());
        }
        return value;
    }

    /**
     * Проверка допустимости присвоения, выполняемая на этапе компиляции.
     * Алгоритм проверки должен соответствовать
     * алгоритму приведения в {@link Value#cast(Type)}.
     */
    public boolean assignableFrom(Type src) {
        if (src == UNDEFINED || this == UNDEFINED) {
            // Контроль неопределённых на этапе компиляции типов невозможен
            return true;
        }
        final BaseType srcBase = src.baseType;
        if (arity > 0) {
            // Для массивов необходимо точное соответствие типов
            return src.getArity() == arity && baseType == srcBase;
        } else {
            if (baseType == srcBase) {
                return true;
            } else {
                // Неявные приведения
                if (baseType == BaseType.NUMBER && srcBase == BaseType.INTEGER) {
                    return true;
                }
            }
            return false;
        }
    }

    public String getSignature() {
        return signature;
    }

    private static <T> String getSignature(BaseType<T> baseType, int arity) {
        return StringUtils.repeat(baseType.getName(), "[", arity);
    }

    public static Type<?> fromSignature(String signature) {
        final int arrMarkerIndex = signature.indexOf('[');
        if (arrMarkerIndex == -1) {
            return make(BaseType.byName(signature), 0);
        } else {
            final int arity = signature.length() - arrMarkerIndex;
            final BaseType baseType = BaseType.byName(signature.substring(0, arrMarkerIndex));
            return make(baseType, arity);
        }
    }

    public int getArity() {
        return arity;
    }

    public BaseType<T> getBase() {
        return baseType;
    }

    @Override
    public String toString() {
        return signature;
    }
}
