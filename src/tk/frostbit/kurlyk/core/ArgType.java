package tk.frostbit.kurlyk.core;

public enum ArgType {

    VALUE,
    SYMBOL,
    ADDRESS,
    ARGC;

}
