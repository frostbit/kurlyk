package tk.frostbit.kurlyk.core;

public interface IClass {

    String getName();

    boolean isNative();

    IMethod getMethod(String signature);

    Value getStaticField(String name);

    Value<IObject> newInstance();

}
