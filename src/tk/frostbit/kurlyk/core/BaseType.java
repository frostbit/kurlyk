package tk.frostbit.kurlyk.core;

import java.util.*;

public class BaseType<T> {

    public static final BaseType<Boolean> BOOL = new Bool();
    public static final BaseType<Integer> INTEGER = new Signed();
    public static final BaseType<Character> SIGN = new Sign();
    public static final BaseType<Double> NUMBER = new Num();
    public static final BaseType<IObject> OBJECT = new Obj();
    public static final BaseType<Void> VOID = new KurVoid();

    private static final BaseType[] TYPES = { BOOL, INTEGER, SIGN, NUMBER, OBJECT, VOID };
    private static final Map<String, BaseType> BY_NAME = new HashMap<>();

    private final Class<T> javaClass;
    private final String name;

    static {
        for (BaseType pt : TYPES) {
            BY_NAME.put(pt.getName(), pt);
        }
    }

    private BaseType(Class<T> javaClass, String name) {
        this.javaClass = javaClass;
        this.name = name;
    }

    public static BaseType<?> byName(String name) {
        return BY_NAME.get(name);
    }

    public T defaultValue() {
        return null;
    }

    public final Class<T> getJavaClass() {
        return javaClass;
    }

    public final String getName() {
        return name;
    }

    public Type<T> asType() {
        return Type.base(this);
    }

    private static final class Bool extends BaseType<Boolean> {
        private Bool() {
            super(Boolean.class, "bool");
        }

        @Override
        public Boolean defaultValue() {
            return Boolean.FALSE;
        }
    }

    private static final class Signed extends BaseType<Integer> {
        private Signed() {
            super(Integer.class, "signed");
        }

        @Override
        public Integer defaultValue() {
            return 0;
        }
    }

    private static final class Sign extends BaseType<Character> {
        private Sign() {
            super(Character.class, "sign");
        }

        @Override
        public Character defaultValue() {
            return Character.MIN_VALUE;
        }
    }

    private static final class Num extends BaseType<Double> {
        private Num() {
            super(Double.class, "number");
        }

        @Override
        public Double defaultValue() {
            return 0.0;
        }
    }

    private static final class Obj extends BaseType<IObject> {
        private Obj() {
            super(IObject.class, "object");
        }
    }

    private static final class KurVoid extends BaseType<Void> {
        private KurVoid() {
            super(Void.class, "void");
        }
    }

}
