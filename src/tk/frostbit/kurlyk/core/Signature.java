package tk.frostbit.kurlyk.core;

import tk.frostbit.kurlyk.compiler.MethodDef;

import java.util.*;

public class Signature {

    public static final String INIT_NAME = "_init";
    public static final String CTOR_NAME = "_ctor";
    public static final String STATIC_INIT_NAME = "_statinit";
    public static final String ARRAY_SIZE_FIELD = "size";
    public static final String CURRENT_OBJECT_IDENT = "this";

    public static String method(String name, List<Type> argTypes) {
        return (argTypes == null) ? method(name, -1) : method(name, argTypes.size());
    }

    public static String method(String name, int argc) {
        final boolean noSuffix = (argc == -1)
                || name.equals(INIT_NAME) || name.equals(STATIC_INIT_NAME);
        return noSuffix ? name : name + "_" + argc;
    }

    public static String globalMethod(String className, String methodSignature) {
        return className + "." + methodSignature;
    }

    public static String globalMethod(String className, String name, int argc) {
        return globalMethod(className, method(name, argc));
    }

    public static String globalMethod(String className, MethodDef method) {
        return globalMethod(className, method.getSignature());
    }

    public static String globalField(IClass clazz, String name) {
        return clazz.getName() + "." + name;
    }

    public static String extractName(String signature) {
        final String globalName = signature.substring(0, signature.lastIndexOf('_'));
        return woClassName(globalName);
    }

    public static String extractClass(String signature) {
        return signature.substring(0, signature.lastIndexOf("."));
    }

    public static String woClassName(String signature) {
        return signature.substring(signature.indexOf('.') + 1);
    }

}
