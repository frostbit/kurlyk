package tk.frostbit.kurlyk.core;

import tk.frostbit.kurlyk.runtime.StackFrame;

import java.util.*;

public interface IMethod {

    boolean isNative();

    boolean isStatic();

    Value callNative(int argc, Stack<Value<?>> stack);

    int call(int argc, Stack<Value<?>> stack, StackFrame outFrame);

    Type getType();

}
