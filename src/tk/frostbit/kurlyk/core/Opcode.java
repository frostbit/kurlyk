package tk.frostbit.kurlyk.core;

public enum Opcode {

    /** Нет операции */
    NOP(null, null, false),
    /** Ссылка на место в исходнике */
    SOURCE(ArgType.SYMBOL, null, false),

    /** Обмен данными между локальными переменными и стеком */
    MOV(ArgType.VALUE, null, true),
    /** Присвоение значения лежащему на вершине стека lvalue */
    ASSIGN(ArgType.VALUE, null, false),
    /** Возврат из метода */
    RET(ArgType.VALUE, null, false),
    /** Вызов статического метода */
    CALL(ArgType.SYMBOL, ArgType.ARGC, true),
    /** Вызов метода экземпляра */
    CALLVIRT(ArgType.SYMBOL, ArgType.ARGC, true),
    /** Создание нового экземпляра объекта */
    NEWOBJ(ArgType.SYMBOL, ArgType.ARGC, true),
    /** Создание массива заданного размера */
    NEWARR(ArgType.SYMBOL, ArgType.VALUE, true),

    /** Безусловный переход */
    JMP(ArgType.ADDRESS, null, false),
    /** Переход, когда на вершине стека false */
    JF(ArgType.ADDRESS, null, false),
    /** Переход, когда заданное значение совпадает с находящимся на вершине стека */
    JE(ArgType.ADDRESS, ArgType.VALUE, false),

    // Результат всегда на стеке
    /** Обращение к индексу массива */
    INDEX(ArgType.VALUE, ArgType.VALUE, false),
    /** Обращение к полю экземпляра объекта */
    OBJFLD(ArgType.SYMBOL, ArgType.VALUE, false),
    /** Обращение к статическому полю */
    STATFLD(ArgType.SYMBOL, null, false),

    /** Сложение */
    ADD(true),
    /** Вычитание */
    SUB(true),
    /** Умножение */
    MUL(true),
    /** Деление */
    DIV(true),
    /** Остаток от деления */
    MOD(true),
    /** Противоположное число (0 - arg) */
    NEG(false),
    /** Проверка на равенство */
    CEQ(true),
    /** Проверка на неравенство */
    CNE(true),
    /** Больше или равно */
    CGE(true),
    /** Меньше или равно */
    CLE(true),
    /** Меньше */
    CL(true),
    /** Больше */
    CG(true),
    /** Логическое И */
    AND(true),
    /** Логическое ИЛИ */
    OR(true),
    /** Логическое НЕ */
    INV(false)
    ;

    public final ArgType arg1type;
    public final ArgType arg2type;
    public final boolean hasResult;

    Opcode(ArgType arg1type, ArgType arg2type, boolean hasResult) {
        this.arg1type = arg1type;
        this.arg2type = arg2type;
        this.hasResult = hasResult;
    }

    Opcode(boolean binary) {
        this(ArgType.VALUE, binary ? ArgType.VALUE : null, true);
    }

}
