package tk.frostbit.kurlyk.core;

public class Pcode {

    public static final int VAL_STACK = -1;

    public Opcode op;
    public int arg1;
    public int arg2;
    public int dest;

    public Pcode(Opcode op) {
        this.op = op;
    }

    public Pcode(Opcode op, int arg1, int arg2, int dest) {
        this.op = op;
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.dest = dest;
    }

    public String dasm(Program prog) {
        final StringBuilder sb = new StringBuilder();
        sb.append(op.toString()).append(" ");
        if (op.hasResult || op.arg2type != null || op.arg1type != null) {
            sb.append(dasmOperand(op.arg1type, arg1, prog)).append(" ");
        }
        if (op.hasResult || op.arg2type != null) {
            sb.append(dasmOperand(op.arg2type, arg2, prog)).append(" ");
        }
        if (op.hasResult) {
            sb.append(dasmValue(dest, prog));
        }
        return sb.toString();
    }

    private static String dasmOperand(ArgType type, int value, Program prog) {
        if (type == null) {
            return "-";
        }
        switch (type) {
            case ADDRESS:
                return "$" + value;
            case ARGC:
                return "*" + value;
            case SYMBOL:
                return "\"" + prog.getSymbol(value) + "\"";
            case VALUE:
                return dasmValue(value, prog);
        }
        throw new RuntimeException();
    }

    private static String dasmValue(int value, Program prog) {
        if (value == VAL_STACK) {
            return "@";
        } else if (value < VAL_STACK) {
            return prog.getConstant(value).toString();
        } else if (value == 0) {
            return "#";
        } else {
            // TODO: print local name
            return "#" + Integer.toString(value);
        }
    }

}
