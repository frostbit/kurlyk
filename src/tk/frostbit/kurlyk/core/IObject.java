package tk.frostbit.kurlyk.core;

public interface IObject {

    IClass clazz();

    Value getField(String name);

}
