package tk.frostbit.kurlyk.core;

import tk.frostbit.kurlyk.runtime.RuntimeError;

import java.util.*;

public class Value<T> {

    public static final Value NULL = Type.UNDEFINED.newValue();
    public static final Value<Boolean> TRUE = new Value<>(BaseType.BOOL, true);
    public static final Value<Boolean> FALSE = new Value<>(BaseType.BOOL, false);
    public static final Value<Void> VOID = new Value<>(BaseType.VOID, null);

    private final Type<T> type;

    private Value<T>[] array;
    private T primValue;

    public Value(Type<T> type) {
        this.type = type;
    }

    public Value(BaseType<T> type) {
        this(Type.base(type));
    }

    public Value(BaseType<T> type, T value) {
        this(type);
        this.primValue = value;
    }

    public Type<T> getType() {
        return type;
    }

    public T getPrimValue() {
        return primValue;
    }

    public void newArray(int size) {
        final int arity = type.getArity();
        if (arity == 0) {
            throw new IllegalArgumentException();
        }
        @SuppressWarnings("unchecked")
        final Value<T>[] array = new Value[size];
        for (int i = 0; i < size; i++) {
            array[i] = Type.make(type.getBase(), arity - 1).newValue();
        }
        this.array = array;
    }

    public int arraySize() {
        return array.length;
    }

    public Value<T> getElement(int index) {
        return array[index];
    }

    public void assign(Object value) {
        if (type.getArity() > 0) {
            if (value == null) {
                array = null;
            } else {
                throw new RuntimeError("Cannot assign value to array");
            }
        }
        final Class<T> javaClass = type.getBase().getJavaClass();
        if (value == null) {
            if (type.getBase() == BaseType.OBJECT) {
                primValue = null;
            } else {
                throw new RuntimeError("Cannot assign null to " + type);
            }
        } else if (type.getBase() == BaseType.NUMBER && value instanceof Integer) {
            final double doubleVal = (Integer) value;
            primValue = javaClass.cast(doubleVal);
        } else if (javaClass.isInstance(value)) {
            primValue = javaClass.cast(value);
        } else {
            throw new RuntimeError("Cannot assign " + value.getClass() + " to " + type);
        }
    }

    public void assign(Value<?> rval) {
        final int arity = type.getArity();
        if (arity > 0) {
            if (rval == NULL) {
                array = null;
            } else if (type.equals(rval.getType())) {
                // Type safety is guaranteed by Type equality
                @SuppressWarnings("unchecked")
                final Value<T>[] ra = (Value<T>[]) rval.array;
                this.array = ra;
            } else {
                throw makeIllegalAssignment(rval);
            }
        } else {
            final BaseType<T> baseType = type.getBase();
            if (rval == NULL && baseType == BaseType.OBJECT) {
                primValue = null;
            } else {
                primValue = rval.cast(type);
            }
        }
    }

    private RuntimeError makeIllegalAssignment(Value rval) {
        return new RuntimeError("Cannot assign " +
                rval.type + " (" + rval + ") to " + type + " (" + this + ")");
    }

    public <V> V cast(Type<V> type) {
        if (type.getArity() == 0) {
            return cast(type.getBase());
        } else {
            throw makeIllegalCast(type);
        }
    }

    public <V> V cast(BaseType<V> type) {
        final BaseType ownType = this.type.getBase();
        if (type == ownType) {
            return type.getJavaClass().cast(primValue);
        }
        // Неявное преобразование целого в вещественное
        if (type == BaseType.NUMBER && ownType == BaseType.INTEGER) {
            final Integer intValue = cast(BaseType.INTEGER);
            final Double doubleVal = (double) intValue;
            return type.getJavaClass().cast(doubleVal);
        }
        throw makeIllegalCast(type.asType());
    }

    private RuntimeError makeIllegalCast(Type destType) {
        return new RuntimeError("Cannot cast " + type + " (" + this + ") to " + destType);
    }

    @Override
    public String toString() {
        if (type.getBase() == BaseType.VOID) {
            return "VOID";
        } else if (this == NULL) {
            return "NULL";
        } else {
            if (type.getArity() == 0) {
                return Objects.toString(primValue);
            } else {
                final Type<T> elemType = Type.make(type.getBase(), type.getArity() - 1);
                return "ARRAY<"+ elemType + ", "
                        + (array == null ? "NULL" : array.length) + ">";
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value<?> value = (Value<?>) o;
        return Objects.equals(type, value.type) &&
                Objects.equals(array, value.array) &&
                Objects.equals(primValue, value.primValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, array, primValue);
    }
}
