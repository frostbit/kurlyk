package tk.frostbit.kurlyk.core;

import tk.frostbit.kurlyk.compiler.CompileError;

import java.io.PrintStream;
import java.util.*;

public class Program {

    public static final int MAX_ID = Pcode.VAL_STACK - 1;

    private final List<String> symbols = new ArrayList<>();
    private final Map<String, Integer> symbolIndexes = new HashMap<>();

    private final List<Value> constants = new ArrayList<>();
    private final Map<Value, Integer> constIndexes = new HashMap<>();

    private final List<Pcode> pcodes = new ArrayList<>();
    private final Map<String, IClass> classes = new HashMap<>();

    private final Queue<IClass> uninitializedClasses = new ArrayDeque<>();

    public int size() {
        return pcodes.size();
    }

    public Pcode pc(int address) {
        return pcodes.get(address);
    }

    public void addInstruction(Pcode pcode) {
        pcodes.add(pcode);
    }

    public void addInstruction(Opcode opcode, int arg1, int arg2, int dest) {
        addInstruction(new Pcode(opcode, arg1, arg2, dest));
    }

    public void addInstruction(Opcode opcode, String symbol, int arg2, int dest) {
        final int symbolId = registerSymbol(symbol);
        addInstruction(opcode, symbolId, arg2, dest);
    }

    public void removeInstruction(int index) {
        pcodes.remove(index);
    }

    public void registerClass(IClass clazz) {
        if (classes.containsKey(clazz.getName())) {
            throw new CompileError("Class already registered: " + clazz.getName());
        } else {
            classes.put(clazz.getName(), clazz);
            if (!clazz.isNative()) {
                uninitializedClasses.add(clazz);
            }
        }
    }

    public IClass getClass(String name) {
        return classes.get(name);
    }

    public Iterable<IClass> getClasses() {
        return classes.values();
    }

    public IClass nextUninitialized() {
        return uninitializedClasses.poll();
    }

    public int registerSymbol(String symbol) {
        return registerEntity(symbol, symbols, symbolIndexes);
    }

    public String getSymbol(int id) {
        return symbols.get(flip(id));
    }

    public int registerConst(Value constant) {
        return registerEntity(constant, constants, constIndexes);
    }

    public Value getConstant(int id) {
        return constants.get(flip(id));
    }

    private static <T> int registerEntity(T entity, List<T> list, Map<T, Integer> map) {
        if (map.containsKey(entity)) {
            return map.get(entity);
        }
        final int index = flip(list.size());
        list.add(entity);
        map.put(entity, index);
        return index;
    }

    private static int flip(int index) {
        return -(index - MAX_ID);
    }

    public void dump(PrintStream out) {
        for (int i = 0; i < pcodes.size(); i++) {
            Pcode pcode = pcodes.get(i);
            out.printf(pcode.op == Opcode.SOURCE ? "" : "%4d: ", i);
            out.println(pcode.dasm(this));
        }
    }

}
