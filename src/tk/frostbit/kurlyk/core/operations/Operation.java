package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;

abstract class Operation {

    protected final String sign;
    protected final Opcode opcode;

    public Operation(String sign, Opcode opcode) {
        this.sign = sign;
        this.opcode = opcode;
    }

    public String getSign() {
        return sign;
    }

    public Opcode getOpcode() {
        return opcode;
    }

    protected static int toInt(Value<?> arg) {
        return arg.cast(BaseType.INTEGER);
    }

    protected static double toNumber(Value<?> arg) {
        return arg.cast(BaseType.NUMBER);
    }

    protected static boolean toBool(Value<?> arg) {
        return arg.cast(BaseType.BOOL);
    }

}
