package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;

import java.util.*;

class EqualityOperation extends BinaryOperation {

    private final boolean xor;

    static final List<EqualityOperation> OPERATIONS = Arrays.asList(
            new EqualityOperation("==", Opcode.CEQ, false),
            new EqualityOperation("!=", Opcode.CNE, true)
    );

    private EqualityOperation(String sign, Opcode opcode, boolean xor) {
        super(sign, opcode);
        this.xor = xor;
    }

    @Override
    public final BaseType typeOfResult(BaseType leftType, BaseType rightType) {
        final boolean lNum = (leftType == BaseType.INTEGER) || (leftType == BaseType.NUMBER);
        final boolean rNum = (rightType == BaseType.INTEGER) || (rightType == BaseType.NUMBER);
        final boolean bools = (leftType == BaseType.BOOL) && (leftType == BaseType.BOOL);
        if (lNum && rNum || bools) {
            return BaseType.BOOL;
        } else {
            return null;
        }
    }

    @Override
    protected final void apply(Value<?> left, Value<?> right, Value<?> dest, BaseType type) {
        final boolean result;
        if (left.getType().getBase() == BaseType.BOOL) {
            result = toBool(left) == toBool(right);
        } else {
            result = toNumber(left) == toNumber(right);
        }
        dest.assign(result ^ xor);
    }

}
