package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;

import java.util.*;

abstract class LogicOperation extends BinaryOperation {

    static final List<LogicOperation> OPERATIONS = new ArrayList<>();

    static {
        OPERATIONS.add(new LogicOperation("&", Opcode.AND) {
            @Override
            protected boolean apply(boolean left, boolean right) {
                return left && right;
            }
        });
        OPERATIONS.add(new LogicOperation("|", Opcode.OR) {
            @Override
            protected boolean apply(boolean left, boolean right) {
                return left || right;
            }
        });
    }

    private LogicOperation(String sign, Opcode opcode) {
        super(sign, opcode);
    }

    @Override
    public final BaseType typeOfResult(BaseType leftType, BaseType rightType) {
        return (leftType == BaseType.BOOL && rightType == BaseType.BOOL) ? BaseType.BOOL : null;
    }

    @Override
    protected final void apply(Value<?> left, Value<?> right, Value<?> dest, BaseType type) {
        dest.assign(apply(toBool(left), toBool(right)));
    }

    protected abstract boolean apply(boolean left, boolean right);

}
