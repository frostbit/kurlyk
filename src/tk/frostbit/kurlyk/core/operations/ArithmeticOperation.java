package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;

import java.util.*;

abstract class ArithmeticOperation extends BinaryOperation {

    static final List<ArithmeticOperation> OPERATIONS = new ArrayList<>();

    static {
        OPERATIONS.add(new ArithmeticOperation("+", Opcode.ADD) {
            @Override
            protected double apply(double left, double right) {
                return left + right;
            }
        });
        OPERATIONS.add(new ArithmeticOperation("-", Opcode.SUB) {
            @Override
            protected double apply(double left, double right) {
                return left - right;
            }
        });
        OPERATIONS.add(new ArithmeticOperation("*", Opcode.MUL) {
            @Override
            protected double apply(double left, double right) {
                return left * right;
            }
        });
        OPERATIONS.add(new ArithmeticOperation("/", Opcode.DIV) {
            @Override
            protected double apply(double left, double right) {
                return left / right;
            }
        });
        OPERATIONS.add(new ArithmeticOperation("%", Opcode.MOD) {
            @Override
            protected double apply(double left, double right) {
                return left % right;
            }
        });
    }

    private ArithmeticOperation(String sign, Opcode opcode) {
        super(sign, opcode);
    }

    @Override
    public BaseType typeOfResult(BaseType leftType, BaseType rightType) {
        final boolean leftOk = leftType == BaseType.INTEGER || leftType == BaseType.NUMBER;
        final boolean rightOk = rightType == BaseType.INTEGER || rightType == BaseType.NUMBER;
        if (leftOk && rightOk) {
            if (leftType == BaseType.NUMBER || rightType == BaseType.NUMBER) {
                return BaseType.NUMBER;
            } else {
                return BaseType.INTEGER;
            }
        } else {
            return null;
        }
    }

    @Override
    protected void apply(Value<?> left, Value<?> right, Value<?> dest, BaseType type) {
        if (type == BaseType.INTEGER) {
            dest.assign((int) apply(toNumber(left), toNumber(right)));
        } else {
            dest.assign(apply(toNumber(left), toNumber(right)));
        }
    }

    protected abstract double apply(double left, double right);

}
