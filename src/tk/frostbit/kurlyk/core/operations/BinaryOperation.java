package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;
import tk.frostbit.kurlyk.runtime.RuntimeError;

import java.util.*;

public abstract class BinaryOperation extends Operation {

    private static Map<Opcode, BinaryOperation> BY_OPCODE = new EnumMap<>(Opcode.class);
    private static Map<String, BinaryOperation> BY_SIGN = new HashMap<>();

    static {
        registerOperations(ArithmeticOperation.OPERATIONS);
        registerOperations(ComparisonOperation.OPERATIONS);
        registerOperations(EqualityOperation.OPERATIONS);
        registerOperations(LogicOperation.OPERATIONS);
    }

    private static void registerOperations(Collection<? extends BinaryOperation> operations) {
        for (BinaryOperation op : operations) {
            BY_OPCODE.put(op.getOpcode(), op);
            BY_SIGN.put(op.getSign(), op);
        }
    }

    public BinaryOperation(String sign, Opcode opcode) {
        super(sign, opcode);
    }

    public static BinaryOperation byOpcode(Opcode opcode) {
        return BY_OPCODE.get(opcode);
    }

    public static BinaryOperation bySign(String sign) {
        return BY_SIGN.get(sign);
    }

    public abstract BaseType typeOfResult(BaseType leftType, BaseType rightType);

    public void apply(Value<?> left, Value<?> right, Value<?> dest) {
        final BaseType type = typeOfResult(left.getType().getBase(), right.getType().getBase());
        if (type == null) {
            throw makeNotAppiable(left, right);
        } else {
            apply(left, right, dest, type);
        }
    }

    protected abstract void apply(Value<?> left, Value<?> right, Value<?> dest, BaseType type);

    protected final RuntimeError makeNotAppiable(Value<?> left, Value<?> right) {
        return new RuntimeError("Operation " + sign + " cannot be applied to "
                + left.getType() + " and " + right.getType());
    }

}
