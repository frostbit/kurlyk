package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.BaseType;
import tk.frostbit.kurlyk.core.Opcode;
import tk.frostbit.kurlyk.core.Value;

import java.util.*;

/**
 * Все операции сравнения чисел.
 */
abstract class ComparisonOperation extends BinaryOperation {

    static final List<ComparisonOperation> OPERATIONS = new ArrayList<>();

    static {
        OPERATIONS.add(new ComparisonOperation(">=", Opcode.CGE) {
            @Override
            protected boolean apply(double left, double right) {
                return left >= right;
            }
        });
        OPERATIONS.add(new ComparisonOperation("<=", Opcode.CLE) {
            @Override
            protected boolean apply(double left, double right) {
                return left <= right;
            }
        });
        OPERATIONS.add(new ComparisonOperation(">", Opcode.CG) {
            @Override
            protected boolean apply(double left, double right) {
                return left > right;
            }
        });
        OPERATIONS.add(new ComparisonOperation("<", Opcode.CL) {
            @Override
            protected boolean apply(double left, double right) {
                return left < right;
            }
        });
    }

    private ComparisonOperation(String sign, Opcode opcode) {
        super(sign, opcode);
    }

    @Override
    public BaseType typeOfResult(BaseType leftType, BaseType rightType) {
        final boolean allOk = (leftType == BaseType.NUMBER || leftType == BaseType.INTEGER)
                && (rightType == BaseType.NUMBER || rightType == BaseType.INTEGER);
        return allOk ? BaseType.BOOL : null;
    }

    @Override
    protected void apply(Value<?> left, Value<?> right, Value<?> dest, BaseType type) {
        final double leftNum = toNumber(left);
        final double rightNum = toNumber(right);
        dest.assign(apply(leftNum, rightNum));
    }

    protected abstract boolean apply(double left, double right);

}
