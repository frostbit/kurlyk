package tk.frostbit.kurlyk.core.operations;

import tk.frostbit.kurlyk.core.*;
import tk.frostbit.kurlyk.runtime.RuntimeError;

import java.util.*;

public abstract class UnaryOperation extends Operation {

    private static final Map<Opcode, UnaryOperation> BY_OPCODE = new EnumMap<>(Opcode.class);
    private static final Map<String, UnaryOperation> BY_SIGN = new HashMap<>();

    static {
        final UnaryOperation[] operations = { new Negation(), new Invert() };
        for (UnaryOperation op : operations) {
            BY_OPCODE.put(op.opcode, op);
            BY_SIGN.put(op.sign, op);
        }
    }

    private UnaryOperation(String sign, Opcode opcode) {
        super(sign, opcode);
    }

    public static UnaryOperation bySign(String sign) {
        return BY_SIGN.get(sign);
    }

    public static UnaryOperation byOpcode(Opcode opcode) {
        return BY_OPCODE.get(opcode);
    }

    public abstract BaseType typeOfResult(BaseType argType);

    protected abstract void apply(Value arg, Value dest, BaseType type);

    public final void apply(Value arg, Value dest) {
        final BaseType<?> type = typeOfResult(arg.getType().getBase());
        if (type == null) {
            throw makeNotAppiable(arg);
        }
        apply(arg, dest, type);
    }

    protected final RuntimeError makeNotAppiable(Value arg) {
        return new RuntimeError("Operation " + sign + " cannot be applied to " + arg.getType());
    }

    static class Invert extends UnaryOperation {

        public Invert() {
            super("!", Opcode.INV);
        }

        @Override
        public BaseType typeOfResult(BaseType argType) {
            if (argType == BaseType.BOOL) {
                return BaseType.BOOL;
            } else {
                return null;
            }
        }

        @Override
        protected void apply(Value arg, Value dest, BaseType type) {
            dest.assign(!toBool(arg));
        }
    }

    static class Negation extends UnaryOperation {

        public Negation() {
            super("-", Opcode.NEG);
        }

        @Override
        public BaseType typeOfResult(BaseType argType) {
            if (argType == BaseType.INTEGER) {
                return BaseType.INTEGER;
            } else if (argType == BaseType.NUMBER) {
                return BaseType.NUMBER;
            } else {
                return null;
            }
        }

        @Override
        protected void apply(Value arg, Value dest, BaseType type) {
            if (type == BaseType.INTEGER) {
                dest.assign(-toInt(arg));
            } else if (type == BaseType.NUMBER) {
                dest.assign(-toNumber(arg));
            }
        }

    }
}
