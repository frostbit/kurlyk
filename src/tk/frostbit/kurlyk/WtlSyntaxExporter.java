package tk.frostbit.kurlyk;

import static java.lang.System.out;

public class WtlSyntaxExporter {

    public static void main(String[] args) throws Exception {
        out.println("Загрузка");
        final String input = Io.readFile(args[0]);
        final String wtlFileName = args[1];
        final String src = WtlTransfer.getFile(wtlFileName);
        out.println("Обработка");
        final String result = processXml(input, src);
        out.println("Выгрузка");
        WtlTransfer.putFile(wtlFileName, result);
    }

    private static String processXml(String input, String src) {
        final String syntax = transformGrammar(input);
        final String syntStartMarker = "<syntax>";
        final String syntEndMarker = "</syntax>";
        final String fileStart = src.substring(
                0, src.indexOf(syntStartMarker) + syntStartMarker.length());
        final String oldSyntax = src.substring(fileStart.length());
        final String dataStartMarker = "<data>";
        final String dataEndMarker = "</data>";
        final int dataStartPos = oldSyntax.indexOf(dataStartMarker);
        final String syntData =
                (dataStartPos == -1) ? "" : oldSyntax.substring(
                        dataStartPos, oldSyntax.indexOf(dataEndMarker) + dataEndMarker.length());
        return fileStart + "\n"
                + syntData + "\n"
                + syntax + "\n"
                + "</syntax>\n</transLab>";
    }

    private static String transformGrammar(String rulesString) {
        final String[] rules = rulesString.split("\r?\n");
        final StringBuilder output = new StringBuilder();
        for (String rule : rules) {
            try {
                if (rule.length() == 0) continue;
                if (rule.startsWith("#")) continue;
                rule = rule.trim().replace("<", "&lt;").replace(">", "&gt;");
                final int colonIndex = rule.indexOf(":");
                final String leftPart = rule.substring(0, colonIndex);
                String rightPart = rule.substring(colonIndex + 1);
                output.append("<rule for='Root' leftPart='").append(leftPart).append("'>\n");
                final int commentIndex = rightPart.indexOf('#');
                final String comment;
                if (commentIndex > -1) {
                    comment = rightPart.substring(commentIndex + 1).trim();
                    rightPart = rightPart.substring(0, commentIndex);
                } else {
                    comment = null;
                }
                output.append("  <rightPart>\n");
                for (String term : rightPart.split("\\s+")) {
                    if (term.length() == 0) continue;
                    if (term.startsWith("\"")) {
                        if (!term.endsWith("\"")) {
                            throw new RuntimeException("Bad input at " + rightPart);
                        }
                        term = term.substring(1, term.length() - 1);
                        putTag("string", term, 2, output);
                    } else if (term.startsWith("$")) {
                        term = term.substring(1);
                        putTag("exclude", term, 2, output);
                    } else if (term.startsWith("@")) {
                        term = term.substring(1);
                        putTag("action", "sc.on" + term + "(currentLexem);", 2, output);
                    } else {
                        putTag("symbol", term, 2, output);
                    }
                }
                output.append("  </rightPart>\n");
                if (comment != null) {
                    output.append("  <comment>\n  ").append(comment).append("\n  </comment>\n");
                }
                output.append("</rule>\n");
            } catch (Exception e) {
                throw new RuntimeException("Failed to parse rule " + rule, e);
            }
        }
        return output.toString();
    }

    private static void putTag(String name, String content, int indent, StringBuilder dest) {
        for (int i = 0; i < indent; i++) {
            dest.append("  ");
        }
        dest.append("<").append(name).append(">");
        dest.append(content);
        dest.append("</").append(name).append(">\n");
    }

}
