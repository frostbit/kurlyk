package tk.frostbit.kurlyk.framework;

import tk.frostbit.kurlyk.core.Program;

/**
 * Предоставляет native классы, написанные на Java.
 */
public final class Framework {

    public static void bootstrap(Program program) {
        program.registerClass(new CoreClass());
        program.registerClass(new IoClass());
    }

}
