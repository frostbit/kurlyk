package tk.frostbit.kurlyk.framework;

import tk.frostbit.kurlyk.core.*;

import java.util.*;

abstract class NativeClass implements IClass {

    protected static final int ARGC_ANY = -1;

    private final String name;
    private final Map<String, IMethod> methods = new HashMap<>();

    NativeClass(String name) {
        this.name = name;
    }

    @Override
    public final IMethod getMethod(String signature) {
        final String name = Signature.extractName(signature);
        if (methods.containsKey(name)) {
            return methods.get(name);
        } else {
            return methods.get(signature);
        }
    }

    protected final void addMethod(String name, IMethod method) {
        addMethod(name, ARGC_ANY, method);
    }

    protected final void addMethod(String name, int argc, IMethod method) {
        methods.put(argc == ARGC_ANY ? name : Signature.method(name, argc), method);
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final boolean isNative() {
        return true;
    }

    @Override
    public Value getStaticField(String name) {
        return null;
    }

    @Override
    public Value<IObject> newInstance() {
        return null;
    }
}
