package tk.frostbit.kurlyk.framework;

import tk.frostbit.kurlyk.core.*;

import java.util.*;

class IoClass extends NativeClass {

    private static final Scanner input = new Scanner(System.in);

    public IoClass() {
        super("Io");
        addMethod("println", new NativeMethod(true, BaseType.VOID.asType()) {
            @Override
            protected Value call(List<Value<?>> args) {
                for (Value val : args) {
                    System.out.print(val);
                }
                System.out.println();
                return Value.VOID;
            }
        });
        addMethod("readInt", 0, new NativeMethod(true, BaseType.INTEGER.asType()) {
            @Override
            protected Value call(List<Value<?>> args) {
                return new Value<>(BaseType.INTEGER, input.nextInt());
            }
        });
        addMethod("print", new NativeMethod(true, BaseType.VOID.asType()) {
            @Override
            protected Value call(List<Value<?>> args) {
                for (Value val : args) {
                    System.out.print(val);
                }
                return Value.VOID;
            }
        });
    }

}
