package tk.frostbit.kurlyk.framework;

import tk.frostbit.kurlyk.core.*;

import java.util.*;

class CoreClass extends NativeClass {

    public CoreClass() {
        super("Core");
        addMethod("charArray", new NativeMethod(true, Type.make(BaseType.SIGN, 1)) {
            @Override
            protected Value call(List<Value<?>> args) {
                final Value<Character> array = new Value<>(Type.make(BaseType.SIGN, 1));
                array.newArray(args.size());
                for (int i = 0; i < args.size(); i++) {
                    array.getElement(i).assign(args.get(i));
                }
                return array;
            }
        });
        addMethod("sleep", 1, new NativeMethod(true, BaseType.VOID.asType()) {
            @Override
            protected Value call(List<Value<?>> args) {
                final int millis = args.get(0).cast(BaseType.INTEGER);
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException ignored) { }
                return Value.VOID;
            }
        });
    }

}
