package tk.frostbit.kurlyk.framework;

import tk.frostbit.kurlyk.core.IMethod;
import tk.frostbit.kurlyk.core.Type;
import tk.frostbit.kurlyk.core.Value;
import tk.frostbit.kurlyk.runtime.StackFrame;

import java.util.*;

abstract class NativeMethod implements IMethod {

    private final boolean isStatic;
    private final Type type;

    public NativeMethod(boolean isStatic, Type type) {
        this.isStatic = isStatic;
        this.type = type;
    }

    public final Value callNative(int argc, Stack<Value<?>> stack) {
        final List<Value<?>> args = new ArrayList<>();
        for (int i = 0; i < argc; i++) {
            args.add(stack.pop());
        }
        return call(args);
    }

    protected abstract Value call(List<Value<?>> args);

    @Override
    public final boolean isNative() {
        return true;
    }

    @Override
    public final boolean isStatic() {
        return isStatic;
    }

    @Override
    public final Type getType() {
        return type;
    }

    @Override
    public final int call(int argc, Stack<Value<?>> stack, StackFrame outFrame) {
        throw new UnsupportedOperationException();
    }

}
