<%@page import="java.io.*, java.util.*" %><%
/*
 * Скрипт для обмена файлами с WTL.
 * Параметр file указывает имя файла относительно корня Tomcat. 
 * - получение файла: GET запрос
 * - получение файла с сохранением: GET запрос с параметром save=1 
 * - загрузка файла: POST запрос с содержимым файла в теле запроса "как есть"
 * - удаление файла: GET запрос с параметром del=1
 */
String fn;
if (request.getAttribute("file") != null) {
    fn = (String) request.getAttribute("file");
} else {
    fn = request.getParameter("file");
}
File file = new File(fn);
if (request.getMethod().equals("POST")) {
    InputStream is = new BufferedInputStream(request.getInputStream());
    OutputStream os = new FileOutputStream(file);
    byte[] buffer = new byte[4096];
    int count = 0;
    int total = 0;
    while ((count = is.read(buffer)) > 0) {
        os.write(buffer, 0, count);
        total += count;
    }
    os.close();
    response.setContentType("text/plain;");
    out.println(total);
} else {
    if (file.isDirectory()) {
        response.setContentType("text/plain; charset=windows-1251");
        for (String nest : file.list()) {
            out.println(nest);
        }
    } else {
        if (request.getParameter("del") != null) {
            file.delete();
            request.setAttribute("file", file.getParent());
            request.getRequestDispatcher("shell.jsp").forward(request, response);
        } else {
            if (request.getParameter("save") != null) {
                response.setHeader("Content-Disposition",
                         "attachment; filename=\"" + file.getName() +"\"");
            }
            response.setContentType(
                    (fn.endsWith("xml") ? "text/plain" : "*/*") + "; charset=windows-1251");    
            InputStream is = new FileInputStream(file);
            OutputStream os = response.getOutputStream();
            byte[] buffer = new byte[4096];
            int count = 0;
            while ((count = is.read(buffer)) > 0) {
                os.write(buffer, 0, count);
            }
            is.close();
        }
    }
}
%>
